import logging
import time
from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

logger = logging.getLogger(__name__)


def produce_logs():
    for i in range(60):
        logger.info(f"Iteration {i}")
        time.sleep(1)


with DAG(
    "produce_logs",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "start_date": datetime(2024, 9, 25),
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A simple DAG that sleeps for 60s while producing logs",
    schedule=None,
) as dag:
    task = PythonOperator(
        task_id="produce_logs",
        python_callable=produce_logs,
        dag=dag,
    )
    task
