import logging
from datetime import date, datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator

from wmf_airflow_common.clients.s3 import get_s3_client

logger = logging.getLogger(__name__)


def purge_old_logs_from_s3() -> int:
    """
    Iterate over every element in the airflow logs bucket and delete all
    logs older than the retention variable.

    """
    s3_client = get_s3_client("s3_dpe")
    s3_log_retention_days = int(Variable.get("s3_log_retention_days"))
    s3_log_bucket = Variable.get("s3_log_bucket")
    s3_paginator = s3_client.get_paginator("list_objects_v2")
    today = date.today()
    to_delete = []

    logging.info(f"Logs older than {s3_log_retention_days} days will be deleted from s3")
    for page in s3_paginator.paginate(Bucket=s3_log_bucket, Prefix="dag_id="):
        for obj in page.get("Contents", []):
            filepath = obj["Key"]
            # filepath is of the format
            # dag_id=addition/run_id=scheduled__2024-09-21T08:00:00+00:00/task_id=add_one_and_two/attempt=1.log
            try:
                task_scheduling_time_str = filepath.split("/")[1].split("__")[1]
                task_scheduling_date = datetime.fromisoformat(task_scheduling_time_str).date()
            except Exception:
                logging.exception(f"Could not parse {filepath}. Skipping.")
                continue
            if today - task_scheduling_date >= timedelta(days=s3_log_retention_days):
                logging.info(f"{filepath} will be deleted")
                to_delete.append({"Key": obj["Key"]})

    logging.info(f"Deleting {len(to_delete)} logfiles")
    s3_client.delete_objects(Bucket=s3_log_bucket, Delete={"Objects": to_delete})
    return len(to_delete)


with DAG(
    "purge_old_logs_from_s3",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "start_date": datetime(2024, 9, 26),
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
        "catchup": False,
    },
    description="A DAG that cleans up task logs stored in S3 older than a configurable retention",
    schedule="@daily",
    tags=["airflow_maintenance"],
) as dag:
    task = PythonOperator(
        task_id="purge_old_logs_from_s3",
        python_callable=purge_old_logs_from_s3,
        dag=dag,
    )
    task
