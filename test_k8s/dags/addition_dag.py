from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator


def add_one_and_two():
    return 1 + 2


with DAG(
    "addition",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "start_date": datetime(2024, 9, 26),
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A simple DAG that computes 1 + 2",
    schedule="@hourly",
) as dag:
    task = PythonOperator(
        task_id="add_one_and_two",
        python_callable=add_one_and_two,
        dag=dag,
    )
    task
