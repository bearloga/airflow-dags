from typing import Any, Dict, Mapping, Optional, Union

from airflow.exceptions import AirflowFailException
from airflow.providers.apache.spark.operators.spark_submit import (
    SparkSubmitOperator as AirflowSparkSubmitOperator,
)
from mergedeep import merge

from wmf_airflow_common.hooks.spark import (
    SparkSkeinSubmitHook,
    SparkSubmitHook,
    kwargs_for_virtualenv,
)
from wmf_airflow_common.util import resolve_kwargs_default_args


class SparkSubmitOperator(AirflowSparkSubmitOperator):
    """
    A SparkSubmitOperator that uses wmf_airflow_common SparkSubmitHook to add
    extra features (like launching via Skein) that are not included with the
    built-in Airflow SparkSubmitOperator.
    """

    template_fields = AirflowSparkSubmitOperator.template_fields + (
        "_command_preamble",
        "_command_postamble",
        "_driver_java_options",
        "_master",
        "_driver_cores",
        "_driver_memory",
        "_executor_cores",
        "_executor_memory"
        # _max_executors will be templated as part of _conf['spark.dynamicAllocation.maxExecutors']
    )

    """
    List of available values for the launcher param.
    """
    available_launchers = ("local", "skein")

    def __init__(
        self,
        launcher: str = "local",
        avoid_local_execution: bool = False,
        command_preamble: Optional[str] = None,
        command_postamble: Optional[str] = None,
        driver_cores: Optional[int] = None,
        # executor_cores: Optional[Union[int, str]] = None,
        driver_java_options: Optional[str] = None,
        master: Optional[str] = "local",
        deploy_mode: Optional[str] = None,
        queue: Optional[str] = None,
        spark_home: Optional[str] = None,
        skein_master_log_level: Optional[str] = None,
        skein_client_log_level: Optional[str] = None,
        skein_app_log_collection_enabled: bool = False,
        conn_id: Optional[str] = None,  # overridden here to change default value
        name: str = "{{ task_instance_key_str }}",  # overridden here to change default value
        # overridden here to support dict k,v pairs.
        application_args: Optional[Union[list, dict]] = None,
        default_env_vars: Optional[Dict[str, str]] = None,
        # Must be provided so BaseOperatorMeta._apply_defaults can
        # propagate the default value from dag default_args before
        # we override any values.
        conf: Optional[Mapping[str, Any]] = None,
        # common values provided in spark conf as kwargs to reduce typos
        # and simplify extending dag default_args conf.
        max_executors: Optional[Union[int, str]] = None,
        sql_shuffle_partitions: Optional[int] = None,
        executor_memory_overhead: Optional[Union[str, int]] = None,
        driver_memory_overhead: Optional[Union[str, int]] = None,
        **kwargs: Any,  # Rest of the kwargs for provided Airflow SparkSubmitHook
    ):
        """
        Initializes a SparkSubmitOperator.  For parameter documentation, see
        wmf_airflow_common SparkSubmitHook and the provided Airflow SparkSubmitHook.

        :param launcher:
            launcher to use to run spark-submit command. Only supported
            launchers are 'local' (default) and 'skein'.
            If 'local', spark-submit will be run locally via our
            SparkSubmitHook.
            If 'skein' which will result in using our SparkSkeinSubmitHook.
            Default: 'local'

        :param command_preamble:
            Command to prefix before the spark-submit command.
            This only works when launcher=skein, else it is ignored.

        :param avoid_local_execution:
            If True, this will raise an AirflowException if we would have
            run spark-submit in a way that would cause local Spark work to
            happen. That is, either launcher must be skein, or deploy_mode
            must be yarn cluster.
            Default: False.

        :param skein_master_log_level:
            Log level for Skein YARN AppMaster.
            Only used if launcher='skein'.

        :param skein_client_log_level:
            Log level for Skein Client.
            Only used if launcher='skein'.

        :param skein_app_log_collection_enabled:
            If YARN AppMaster logs should be collected and logged locally
            after the app finishes.
            Only used if launcher='skein'.

        :param application_args:
            If a dict of k=v pairs, will be converted to a list.

        :param default_env_vars:
            Default environment variables for spark-submit.
            It provides defaults to the optional env_vars dictionary offered by
            AirflowSparkSubmitOperator.__init__ .

        :param conf:
            Arbitrary Spark configuration properties (templated)

        :param max_executors:
            Upper bound for the number of executors if dynamic allocation is
            enabled. Overrides spark.dynamicAllocation.maxExecutors in conf.

        :param sql_shuffle_partitions:
            The default number of partitions to use when shuffling data for
            joins or aggregations. Overrides spark.sql.shuffle.partitions
            in conf.

        :param executor_memory_overhead:
            Amount of additional memory to be allocated per executor process,
            in MiB unless otherwise specified. Overrides
            spark.executor.memoryOverhead in conf.

        :param driver_memory_overhead:
            Amount of non-heap memory to be allocated per driver process in
            cluster mode, in MiB unless otherwise specified. Overrides
            spark.driver.memoryOverhead in conf.
        """

        self._launcher = launcher
        self._command_preamble = command_preamble
        self._command_postamble = command_postamble
        self._driver_cores = driver_cores
        self._driver_java_options = driver_java_options
        self._master = master
        self._deploy_mode = deploy_mode
        self._queue = queue
        self._spark_home = spark_home
        self._skein_master_log_level = skein_master_log_level
        self._skein_client_log_level = skein_client_log_level
        self._skein_app_log_collection_enabled = skein_app_log_collection_enabled

        if self._launcher not in self.available_launchers:
            raise AirflowFailException(
                f'launcher must be one of {",".join(self.available_launchers)}, ' f"was: {self._launcher}"
            )

        if avoid_local_execution and self._launcher != "skein" and self._deploy_mode != "cluster":
            raise AirflowFailException(
                "Avoiding local Spark execution. " "Please use either launcher='skein' or deploy_mode='cluster'."
            )

        # Support application_args being provided as a dict.
        # Parent SparkSubmitOperator wants them as a list.
        if isinstance(application_args, dict):
            application_args_list = []
            for key, val in application_args.items():
                application_args_list += [key, val]
            application_args = application_args_list

        kwargs["env_vars"] = merge({}, (default_env_vars or {}), kwargs.get("env_vars", {}))

        if conf is None:
            conf = {}
        else:
            conf = dict(conf)
        if max_executors:
            conf["spark.dynamicAllocation.maxExecutors"] = max_executors
        if sql_shuffle_partitions:
            conf["spark.sql.shuffle.partitions"] = sql_shuffle_partitions
        if executor_memory_overhead:
            conf["spark.executor.memoryOverhead"] = executor_memory_overhead
        if driver_memory_overhead:
            conf["spark.driver.memoryOverhead"] = driver_memory_overhead
        kwargs["conf"] = conf

        super().__init__(
            # These need to be passed explicitly
            # since we override their default values.
            conn_id=conn_id,
            name=name,
            application_args=application_args,
            deploy_mode=self._deploy_mode,
            # The rest of parent Airflow SparkSubmitOperator kwargs can be passed like this.
            **kwargs,
        )

    def _get_hook(self) -> SparkSubmitHook:
        """
        Overrides the provided Aiflow SparkSubmitHook
        to use our custom SparkSubmitHook.

        :return: a SparkSubmitHook instance
        :rtype: wmf_airflow_common.hooks.SparkSubmitHook
        """

        hook_kwargs = {
            "application": self.application,
            "driver_cores": self._driver_cores and int(self._driver_cores),
            "driver_java_options": self._driver_java_options,
            "master": self._master,
            "deploy_mode": self._deploy_mode,
            "queue": self._queue,
            "spark_home": self._spark_home,
            "conf": self.conf,
            "conn_id": self._conn_id,
            "files": self.files,
            "py_files": self.py_files,
            "archives": self._archives,
            "driver_class_path": self.driver_class_path,
            "jars": self.jars,
            "java_class": self._java_class,
            "packages": self.packages,
            "exclude_packages": self.exclude_packages,
            "repositories": self._repositories,
            "total_executor_cores": self._total_executor_cores,
            "executor_cores": self._executor_cores and int(self._executor_cores),
            "executor_memory": self._executor_memory,
            "driver_memory": self._driver_memory,
            "keytab": self.keytab,
            "principal": self.principal,
            "proxy_user": self.proxy_user,
            "name": self.name,
            "num_executors": self._num_executors,
            "status_poll_interval": self._status_poll_interval,
            "application_args": self.application_args,
            "env_vars": self.env_vars,
            "verbose": self._verbose,
            "spark_binary": self._spark_binary,
        }

        if self._launcher == "skein":
            hook_kwargs.update(
                {
                    "command_preamble": self._command_preamble,
                    "command_postamble": self._command_postamble,
                    "skein_master_log_level": self._skein_master_log_level,
                    "skein_client_log_level": self._skein_client_log_level,
                    "skein_app_log_collection_enabled": self._skein_app_log_collection_enabled,
                }
            )
            hook = SparkSkeinSubmitHook(**hook_kwargs)
        else:
            hook = SparkSubmitHook(**hook_kwargs)

        return hook

    @classmethod
    def for_virtualenv(
        cls,
        virtualenv_archive: str,
        entry_point: str,
        use_virtualenv_python: bool = True,
        use_virtualenv_spark: bool = False,
        **kwargs: Any,
    ) -> "SparkSubmitOperator":
        """
        Factory method for using a SparkSubmitOperator with a virtualenv.
        This should work with both Conda and regular virtualenvs.

        Example:
        ::
            op = SparkSubmitOperator.for_virtualenv(
                # This will by default be unpacked on the
                # workers as 'venv'.  You can change this by setting
                # an '#my_alias_here' suffix on this archive url.
                virtualenv_archive = 'hdfs:///path/to/my_conda_env.tgz#env_alias',

                # Path to the pyspark job file you want to execute, relative
                # to the archive.  (Note that this must end with '.py' for spark-submit
                # to realize it is a pyspark job.)
                entry_point = 'bin/my_pyspark_job.py'

                # This causes PYSPARK_PYTHON, etc. to be set to <alias>/bin/python
                use_virtualenv_python = True,

                # This causes spark_binary to be set to <alias>/bin/spark-submit.
                # Only set this if you include pyspark as a dependency package in your venv.
                use_virtualenv_spark = True,
            )

        :param virtualenv_archive:
            URL to the archive virtualenv file. If no '#alias' suffix is provided,
            The unpacked alias will be 'venv'.

        :param entry_point:
            This should be a relative path to your spark job file
            inside of the virtualenv_archive.

        :param use_virtualenv_python:
            Whether python in side of the venv should be used.
            defaults to True

        :param use_virtualenv_spark:
            Whether bin/spark-submit inside of the venv should be used.
            defaults to False.
            Note that if you set this to true, a spark_home and spark_binary
            kwargs will be overriden.

        Note that skein will be the default 'launcher' if not explicitly set in kwargs.

        :return: SparkSubmitOperator
        """

        # Default launcher=skein.
        # This will usually be what is needed to use a virtualenv archive,
        # as it is not expected that an unpacked virtualenv archive will be
        # available locally.
        if "launcher" not in kwargs:
            kwargs["launcher"] = "skein"

        # NOTE: This uses to SparkSubmitHook.kwargs_for_virtualenv to
        # construct a SparkSubmitOperator for a virtualenv.  This works because
        # the kwargs for SparkSubmitHook are the same as those for SparkSubmitOpeator.
        # This is done so that SparkSubmitHook itself can use this helper as well.
        # This ONLY works here because SparkSubmitHook and SparkSubmitOperator use
        # the same kwarg names, as SparkSubmitOperator is kind of a proxy
        # For SparkSubmitHook.
        spark_submit_kwargs = kwargs_for_virtualenv(
            virtualenv_archive=virtualenv_archive,
            entry_point=entry_point,
            use_virtualenv_python=use_virtualenv_python,
            use_virtualenv_spark=use_virtualenv_spark,
            **kwargs,
        )

        return cls(**spark_submit_kwargs)


class SparkSqlOperator(SparkSubmitOperator):
    """
    Executes a query in SparkSQL using WMFSparkSQLCLIDriver (by default),
    which is a subclass of org.apache.spark.sql.hive.thriftserver.SparkSQLCLIDriver.
    Doing this avoids shelling out to spark-sql,
    allowing us to subclass our SparkSubmitOperator.

    This class attempts to keep the same interface as the built-in Airflow
    SparkSqlOperator, even though it does not extend from it.
    It adds a query_parameters param to aide in correctly populating
    application_args with -d k=v query parameters to SparkSQLCLIDriver.

    However, Airflow templated .sql/.hql files are not supported.
    WMF tries to keep job code separate from scheduler code, so
    .sql files could be anywhere (perhaps already in HDFS!),
    and Airflow is not able to read the .sql file to render it as
    a template.
    To use Airflow template values in your SQL file, use them in
    query_parameters instead, and make sure your SQL file expects
    query variables to be defined that way, instead of using Airflow templates
    directly in your SQL.

    Note that WMFSparkSQLCLIDriver supports specifying SQL files as an HTTPS
    URL. Thus, you can pull your SQL statements from GitLab/GitHub by using their raw
    file rendering APIs.

    Note further that if you change java_class (perhaps to use your own version
    of SparkSQLCLIDriver like we do at WMF), you MUST set the application
    param to the jar containing your class.
    """

    template_fields = SparkSubmitOperator.template_fields + ("_sql", "_query_parameters")

    def __init__(
        self,
        sql: str,
        query_parameters: Optional[Dict[str, Union[str, int]]] = None,
        **kwargs: Any,
    ):
        """
        :param sql:
            The SQL query to execute, or the path to a .sql or .hql file. (templated)
            The path can be an HTTPS URL, thus, you can pull your SQL statements from
            GitLab/GitHub by using their raw file rendering APIs.

        :param query_parameters:
            Dict of query parameters to pass in application_args as '-d'. (templated)

        :param **kwargs:
            kwargs to pass to parent SparkSubmitOperator.
            kwargs['application_args'] is modified to use sql and query_parameters,
            so you probably shouldn't set this kwarg.

        """
        self._sql = sql
        self._query_parameters = query_parameters

        spark_sql_driver_class = "org.apache.spark.sql.hive.thriftserver.WMFSparkSQLCLIDriver"
        """
        Default value of java_class if not set. This subclass of
        org.apache.spark.sql.hive.thriftserver.SparkSQLCLIDriver avoids a bug where HTTPS URLs with
        url-encoded bits get mangled up. See https://phabricator.wikimedia.org/T333001.
        """
        spark_sql_driver_jar_path = resolve_kwargs_default_args(kwargs, "spark_sql_driver_jar_path")
        """
        Default value of application if not set. This should typically be set in the Airflow instance
        dag_config.py.
        """

        # Set default_args in kwargs if it doesn't exist to use it later
        if "default_args" not in kwargs:
            kwargs["default_args"] = {}

        # Only set a default java_class if no default is set
        if "java_class" not in kwargs["default_args"]:
            kwargs["default_args"]["java_class"] = spark_sql_driver_class

        # Only set a default application if no default is set
        if "application" not in kwargs["default_args"]:
            kwargs["default_args"]["application"] = spark_sql_driver_jar_path

        # If using a custom java_class, make sure application is set.
        final_java_class = resolve_kwargs_default_args(kwargs, "java_class")
        final_application = resolve_kwargs_default_args(kwargs, "application")
        if final_java_class != spark_sql_driver_class and final_application == spark_sql_driver_jar_path:
            raise AirflowFailException(
                "If using a custom java_class with SparkSqlOperator, " "the application parameter must be set."
            )
        elif final_java_class == spark_sql_driver_class and spark_sql_driver_jar_path is None:
            raise AirflowFailException(
                "Parameter 'spark_sql_driver_jar_path' must be set. "
                "It should typically be defined in the Airflow instance dag_config.py."
            )

        # NOTE: This works since application_args are not set in default_args
        if "application_args" not in kwargs:
            kwargs["application_args"] = []

        # Pass the sql parameter as either the -f or the -e application_args.
        self._sql = self._sql.strip()
        if self._sql.endswith(".sql") or self._sql.endswith(".hql") or self._sql.startswith("https://"):
            kwargs["application_args"] += ["-f", sql]
        else:
            kwargs["application_args"] += ["-e", sql]

        # Pass query_parameters as -d k=v application_args.
        if self._query_parameters:
            for key, val in self._query_parameters.items():
                kwargs["application_args"] += ["-d", f"{key}={val}"]

        super().__init__(
            **kwargs,
        )
