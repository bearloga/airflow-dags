from datetime import timedelta
from typing import Any

from airflow.plugins_manager import AirflowPlugin
from airflow.timetables.base import DagRunInfo, DataInterval, TimeRestriction, Timetable
from dateutil.relativedelta import relativedelta
from isodate import duration_isoformat, parse_duration
from pendulum import UTC, Date, DateTime, Time


class DelayedTimetable(Timetable):
    """
    Abstract base class for delayed timetables.
    Defines the constructor and the serialization/deserialization methods.
    Assumes the only instance value is the delay field.
    """

    @classmethod
    def deserialize(cls, data: dict[str, Any]) -> Timetable:
        """
        Deserializes the given serialized representation
        into an instance of Timetable.
        This is required by Airflow for this Timetable to work.
        """
        return cls(parse_duration(data["delay"]))

    def __init__(self, delay: timedelta) -> None:
        """
        Initializes the DelayedTimetable object.

        :param delay:
            Timedelta that the DAG will wait
            until a daily interval is scheduled.
        """
        if delay.total_seconds() < 0:
            raise ValueError("The delay can not be negative.")
        self.delay = delay

    def serialize(self) -> dict[str, Any]:
        """
        Serializes this instance into a dict-based representation.
        This is required by Airflow for this Timetable to work.
        """
        return {"delay": duration_isoformat(self.delay)}


class DelayedHourlyTimetable(DelayedTimetable):
    """
    Schedules DAG runs on an hourly schedule, but it waits for a given delay before doing so.
    Example usage:
        with DAG(
            ...
            schedule=DelayedHourlyTimetable(timedelta(hours=2)),
            ...
        ) as dag:
            ...
    """

    def __init__(self, delay: timedelta) -> None:
        """
        Initializes a DelayedHourlyTimetable object.
        """
        super().__init__(delay)

    def schedule_compatibility(self) -> str:
        """
        Our datasets need a basic schedule to wait for the appropriate partitions.
        """
        return "@hourly"

    def infer_manual_data_interval(self, run_after: DateTime) -> DataInterval:
        """
        When a DAG run is manually triggered, Airflow uses this method
        to get the DAG run's data interval.
        """
        end_datetime = (run_after - self.delay).replace(minute=0, second=0, microsecond=0, tzinfo=UTC)
        start_datetime = end_datetime - timedelta(hours=1)
        return DataInterval(start=start_datetime, end=end_datetime)

    def next_dagrun_info(
        self,
        *,
        last_automated_data_interval: DataInterval | None,
        restriction: TimeRestriction,
    ) -> DagRunInfo | None:
        """
        When a DAG is enabled, Airflow uses this method to determine which dag_run should be triggered next. It should
        return the next dag_run, depending on the last time it ran (if any), the start_date, the end_date, the
        specified delay and whether catchup is enabled.
        """
        # If the DAG has no start date, don't schedule.
        if restriction.earliest is None:
            return None

        # Calculate the next DAG run's start datetime.
        if last_automated_data_interval is not None:
            if restriction.catchup:
                next_start = last_automated_data_interval.end
            else:
                next_start = max(last_automated_data_interval.end, self._next_start_with_catchup())
        else:
            # Crop the DAG's start date to the earliest hour.
            if (
                restriction_earliest_hour_start := restriction.earliest.replace(
                    minute=0, second=0, microsecond=0, tzinfo=UTC
                )
            ) == restriction.earliest:
                earliest_start = restriction.earliest
            else:
                earliest_start = restriction_earliest_hour_start + timedelta(hours=1)

            # Adjust the DAG's earliest start if catch-up is not supported.
            if not restriction.catchup:
                earliest_start = max(earliest_start, self._next_start_with_catchup())

            next_start = earliest_start

        # If the DAG has an expired end time, don't schedule.
        if restriction.latest is not None and next_start > restriction.latest:
            return None

        # Calculate the next end datetime and run after values.
        next_end = next_start + timedelta(hours=1)
        run_after = next_end + self.delay

        return DagRunInfo(
            run_after=run_after,
            data_interval=DataInterval(next_start, next_end),
        )

    def _next_start_with_catchup(self):
        return (DateTime.now() - timedelta(hours=1) - self.delay).replace(minute=0, second=0, microsecond=0, tzinfo=UTC)


class DelayedDailyTimetable(DelayedTimetable):
    """
    Schedules DAG runs on a daily schedule,
    but it waits for a given delay before doing so.
    This Timetable is useful for sanitization DAGs or data deletion DAGs,
    which operate on data i.e. 90 days after its collection.
    Example usage:
        with DAG(
            ...
            schedule=DelayedDailyTimetable(timedelta(days=90)),
            ...
        ) as dag:
            ...
    """

    def __init__(self, delay: timedelta) -> None:
        """
        Initializes a DelayedDailyTimetable object.
        """
        super().__init__(delay)

    def schedule_compatibility(self) -> str:
        """
        Our datasets need a basic schedule to wait for the appropriate partitions.
        """
        return "@daily"

    def infer_manual_data_interval(self, run_after: DateTime) -> DataInterval:
        """
        When a DAG run is manually triggered, Airflow uses this method
        to get the DAG run's data interval.
        """
        end_date = (run_after - self.delay).date()
        end_datetime = DateTime.combine(end_date, Time.min).replace(tzinfo=UTC)
        start_datetime = end_datetime - timedelta(days=1)
        return DataInterval(start=start_datetime, end=end_datetime)

    def next_dagrun_info(
        self,
        *,
        last_automated_data_interval: DataInterval | None,
        restriction: TimeRestriction,
    ) -> DagRunInfo | None:
        """
        When a DAG is enabled, Airflow uses this method to determine
        which dag_run should be triggered next. It should return the next
        dag_run, depending on the last time ir ran (if any), the start_date,
        the end_date, the specufied delay and whether catchup is enabled.
        """
        # If the DAG has no start date, don't schedule.
        if restriction.earliest is None:
            return None

        # Calculate the next DAG run's start datetime.
        if last_automated_data_interval is not None:
            next_start = last_automated_data_interval.end
        else:
            # Crop the DAG's start date to the earliest start of day.
            if restriction.earliest.time() == Time.min:
                earliest_start = restriction.earliest
            else:
                day_after_start_date = restriction.earliest.date() + timedelta(days=1)
                earliest_start = DateTime.combine(day_after_start_date, Time.min).replace(tzinfo=UTC)

            # Adjust the DAG's earliest start if catch-up is not supported.
            if not restriction.catchup:
                delayed_date = Date.today() - self.delay
                delayed_start = DateTime.combine(delayed_date, Time.min).replace(tzinfo=UTC)
                earliest_start = max(earliest_start, delayed_start)

            next_start = earliest_start

        # If the DAG has an expired end date, don't schedule.
        if restriction.latest is not None and next_start > restriction.latest:
            return None

        # Calculate the next end datetime and run after values.
        next_end = next_start + timedelta(days=1)
        run_after = next_end + self.delay

        return DagRunInfo(
            run_after=run_after,
            data_interval=DataInterval(next_start, next_end),
        )


class DelayedMonthlyTimetable(DelayedTimetable):
    """
    Schedules DAG runs on a monthly schedule,
    but it waits for a given delay before doing so.
    This Timetable is useful for sanitization DAGs or data deletion DAGs,
    which operate on data i.e. 90 days after its collection.
    Example usage:
        with DAG(
            ...
            schedule=DelayedMonthlyTimetable(timedelta(days=90)),
            ...
        ) as dag:
            ...
    """

    def __init__(self, delay: timedelta) -> None:
        """
        Initializes a DelayedMonthlyTimetable object.
        """
        super().__init__(delay)

    def schedule_compatibility(self) -> str:
        """
        Our datasets need a basic schedule to wait for the appropriate partitions.
        """
        return "@monthly"

    def infer_manual_data_interval(self, run_after: DateTime) -> DataInterval:
        """
        When a DAG run is manually triggered, Airflow uses this method
        to get the DAG run's data interval.
        """
        end_date = (run_after - self.delay).replace(day=1).date()
        end_datetime = DateTime.combine(end_date, Time.min).replace(tzinfo=UTC)
        start_datetime = end_datetime - relativedelta(months=1)
        return DataInterval(start=start_datetime, end=end_datetime)

    def next_dagrun_info(
        self,
        *,
        last_automated_data_interval: DataInterval | None,
        restriction: TimeRestriction,
    ) -> DagRunInfo | None:
        """
        When a DAG is enabled, Airflow uses this method to determine
        which dag_run should be triggered next. It should return the next
        dag_run, depending on the last time ir ran (if any), the start_date,
        the end_date, the specufied delay and whether catchup is enabled.
        """
        # If the DAG has no start date, don't schedule.
        if restriction.earliest is None:
            return None

        # Calculate the next DAG run's start datetime.
        if last_automated_data_interval is not None:
            next_start = last_automated_data_interval.end
        else:
            # Crop the DAG's start date to the earliest start of day.
            if restriction.earliest.day == 1 and restriction.earliest.time() == Time.min:
                earliest_start = restriction.earliest
            else:
                month_after_start_date = restriction.earliest.replace(day=1).date() + relativedelta(months=1)
                earliest_start = DateTime.combine(month_after_start_date, Time.min).replace(tzinfo=UTC)

            # Adjust the DAG's earliest start if catch-up is not supported.
            if not restriction.catchup:
                delayed_date = (Date.today() - self.delay).replace(day=1)
                delayed_start = DateTime.combine(delayed_date, Time.min).replace(tzinfo=UTC)
                earliest_start = max(earliest_start, delayed_start)

            next_start = earliest_start

        # If the DAG has an expired end date, don't schedule.
        if restriction.latest is not None and next_start > restriction.latest:
            return None

        # Calculate the next end datetime and run after values.
        next_end = next_start + relativedelta(months=1)
        run_after = next_end + self.delay

        return DagRunInfo(
            run_after=run_after,
            data_interval=DataInterval(next_start, next_end),
        )


class DelayedTimetablesPlugin(AirflowPlugin):
    name = "delayed_timetables"
    timetables = [DelayedDailyTimetable, DelayedMonthlyTimetable, DelayedHourlyTimetable]
