"""
Common Airflow DAG default_args for use at WMF.

This file varies the default_args returned based on a global
'airflow environment', controlled by the util.airflow_environment_name()
function.  There are base default args that will always be returned
(in dev envs and everywhere), and there are default_args that will
only be returned in WMF 'production' environments.

Note about Airflow DAG default_args. default_args are meant to be applied
at a DAG level, which means that every Airflow Operator used within that DAG
will be passed every default arg.  If two Operators have the same parameter
with different meanings (e.g. a generic parameter like 'conf' or something),
and that parameter is in default_args, both Operators will be passed the
same parameter value (unless overridden when instantiating the Operator).
So: be careful.  Make sure you are passing the default_args you intend
when instantiating Operators.
"""

import getpass
import logging
import os
import shutil
from datetime import timedelta
from types import MappingProxyType as immutable_dict
from typing import Any, Dict, Optional

from mergedeep import merge

from wmf_airflow_common.util import (
    airflow_config_get,
    airflow_environment_name,
    is_wmf_airflow_instance,
)


def get_base_spark_defaults_args() -> dict:
    """
    Gets values for some Spark operator defaults we might always want to set.
    """
    spark_default_args: Dict[str, Any] = {}

    # Spark home is set when launching the spark-binary
    # Yet, you can override it with an env variable.
    spark_home = os.environ.get("SPARK_HOME")
    if spark_home:
        spark_default_args["spark_home"] = spark_home
    else:
        # Use WMF custom installed Spark 3
        spark_default_args["spark_binary"] = "spark3-submit"

    # Default to running spark in yarn client mode if hadoop is available.
    # (Assuming this means hadoop is configured for use with Spark by default.)
    if shutil.which("hadoop") is not None:
        spark_default_args["master"] = "yarn"
        spark_default_args["deploy_mode"] = "client"

    # `default_env_vars` is a default dictionary of environment variables set when launching a Spark
    # application. It could be overloaded with the apache-airflow-providers-apache-spark provided
    # `env_vars` variable (templated).
    spark_default_args["default_env_vars"] = {
        # Set Spark 3 conf dir. It's not needed when running Spark 3 from conda-analytics,
        # as the conf dir is linked inside of SPARK_HOME. But, it's a useful default when launching
        # a Spark application from a custom conda environment.
        "SPARK_CONF_DIR": "/etc/spark3/conf",
        # When running skein spark jobs in client mode, if we do not set SPARK_HOME, we cannot find any custom
        # jars, such as the iceberg jars. See T336800.
        "SPARK_HOME": spark_home or "/usr/lib/spark3",
    }

    return spark_default_args


# default_args we should always use, even in dev environments.
base_default_args = immutable_dict(
    {
        # BaseOperator default_args:
        "email_on_retry": False,
        "email_on_failure": False,
        "retries": 5,
        "retry_delay": timedelta(minutes=5),
        "max_active_tis_per_dag": 3,
        "do_xcom_push": False,
        # Depending on the previous task run (or downstream of it) could trigger
        # deadlock. Please add it with caution on a per task basis.
        "depends_on_past": False,
        "wait_for_downstream": False,
        # BaseSensor default args:
        "mode": "reschedule",
        "poke_interval": timedelta(minutes=10).total_seconds(),
        # SparkSubmitOperator defaults we always use.
        **get_base_spark_defaults_args(),
    }
)


def configure_kerberos() -> dict:
    """
    :return: a dict with the keytab and principals setup.
    """
    config = {}
    # For wmf default_args, set default kerberos keytab and principal that might be used
    # by Operators to the one used by this Airflow instance.
    # This allows users to not have to know where their WMF Airflow instance user's
    # keytab is.
    airflow_kerberos_keytab = airflow_config_get("kerberos", "keytab")
    if airflow_kerberos_keytab is not None:
        config["keytab"] = airflow_kerberos_keytab

    airflow_kerberos_principal = airflow_config_get("kerberos", "principal")
    if airflow_kerberos_principal is not None:
        config["principal"] = airflow_kerberos_principal

    return config


# default_args to use in all WMF airflow instances (including dev instances).
wmf_base_default_args = immutable_dict(
    merge(
        {},
        base_default_args,
        {
            # SparkSubmitOperator default_args:
            "driver_cores": 2,
            "driver_memory": "4G",
            "executor_cores": 2,
            "executor_memory": "4G",
            "master": "yarn",
            "deploy_mode": "client",
            "queue": "default",
            "conf": {
                "spark.dynamicAllocation.enabled": "true",
                "spark.dynamicAllocation.maxExecutors": "16",
                "spark.shuffle.service.enabled": "true",
                # We don't override memoryOverhead by default
                # Keeping the line here to facilitate copy-pasting on
                # individual jobs
                # 'spark.yarn.executor.memoryOverhead': '2048',
                "spark.yarn.maxAppAttempts": "1",
            },
        },
        configure_kerberos(),
    )
)


# default_args to use in production WMF airflow instances.
wmf_prod_base_default_args = immutable_dict(
    merge(
        {},
        wmf_base_default_args,
        {
            # BaseOperator default_args:
            "email_on_failure": True,
            # avoid_local_execution keeps
            # SparkSubmitOperator from being used in a way
            # that could cause work to happen locally to airflow.
            "avoid_local_execution": True,
            # Use skein in yarn client mode by default.
            "launcher": "skein",
        },
    )
)


# Different default_args to use in different airflow environments.
# Keyed by enviroment name (automatically determined).
airflow_environment_default_args = immutable_dict(
    {
        # _default_ will be used if airflow environment name is not set.
        "_default_": base_default_args,
        # dev_wmf is to be used by WMF's Airflow dev_instance script.
        "dev_wmf": wmf_base_default_args,
        # 'wmf' is a special_case and will be used if
        # is_wmf_airflow_environment is true, I.e. if
        # airflow environment name starts with 'wmf'.
        "wmf": wmf_prod_base_default_args,
    }
)

# config for jobs that require read or write to the AQS Cassandra cluster.
cassandra_default_conf = immutable_dict(
    {
        "spark.sql.catalog.aqs": "com.datastax.spark.connector.datasource.CassandraCatalog",
        "spark.sql.catalog.aqs.spark.cassandra.connection.host": "aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042",  # noqa: E501
        "spark.sql.catalog.aqs.spark.cassandra.auth.username": "aqsloader",
        "spark.sql.catalog.aqs.spark.cassandra.auth.password": "cassandra",
        "spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows": 1024,
        "spark.sql.catalog.aqs.spark.cassandra.connection.ssl.enabled": "true",
        "spark.sql.catalog.aqs.spark.cassandra.connection.ssl.trustStore.path": "/etc/ssl/localcerts/wmf-java-cacerts",
        "spark.sql.catalog.aqs.spark.cassandra.connection.ssl.enabledAlgorithms": "TLS_AES_256_GCM_SHA384,TLS_AES_128_GCM_SHA256",  # noqa: E501
    }
)


# Config for jobs that ingest to the Druid cluster.
druid_default_conf = immutable_dict(
    {
        "druid_host": "an-druid1002.eqiad.wmnet",
        "druid_port": "8090",
        "druid_api_host": "an-druid1002.eqiad.wmnet",
        "druid_api_port": "8081",
    }
)


# Our cluster defaults to Spark 3.1.2.
# The config below can be used to override to use Spark 3.3.2
username = getpass.getuser()  # avoid ivy errors by making cache location unique per service user
spark_3_3_2_conf = immutable_dict(
    {
        # use the Spark Suffle Service for Spark 3.3
        "spark.shuffle.service.name": "spark_shuffle_3_3",
        "spark.shuffle.service.port": "7338",
        # override the assembly file
        "spark.yarn.archive": "hdfs:///user/spark/share/lib/spark-3.3.2-assembly.zip",
        # extras to make Iceberg work on 3.3.2. Use Iceberg 1.6.1 to pickup https://github.com/apache/iceberg/pull/9101
        "spark.jars.packages": "org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.6.1",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }
)


def get(extra_default_args: Optional[dict] = None) -> immutable_dict:
    """
    Gets dag/operator default_args for airflow instance_name.

    Returned default args might be different depending on the 'environment'
    we are running in.  On WMF airflow instances, we want to apply
    different default_args than we might in development envs.
    The 'environment' is automatically determined if the
    AIRFLOW_ENVIRONMENT_NAME env var is set. (util.airflow_environment_name()
    will return 'wmf' if AIRFLOW_INSTANCE_NAME is set but AIRFLOW_ENVIRONMENT_NAME is not.)
    Whatever it returns, if airflow_environment_name() starts with 'wmf',
    'wmf' specific default_args will be returned.

    :param extra_default_args:
        These will be merged over any of the variant's default_args.
        Dicts are deep merged, any other type is replaced on conflict.

    """

    env_name = airflow_environment_name()
    default_args_set_name = "_default_"
    if env_name is not None and env_name in airflow_environment_default_args:
        default_args_set_name = env_name
    if is_wmf_airflow_instance():
        default_args_set_name = "wmf"
    common_default_args = airflow_environment_default_args[default_args_set_name]
    logging.info("Dag-default-args set is `%s`.", default_args_set_name)

    if extra_default_args is None:
        extra_default_args = {}

    return immutable_dict(
        merge(
            {},
            common_default_args,
            extra_default_args,
        )
    )
