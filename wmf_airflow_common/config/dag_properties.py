import inspect
import os
from datetime import datetime, timedelta
from json import JSONDecodeError
from typing import Any, Dict, List, Optional, Tuple, cast

from airflow.models import Variable
from isodate import duration_isoformat, parse_duration


def _find_vcs_root(test: str, dirs: Tuple[str] = (".git",), default: Optional[str] = None) -> Optional[str]:
    """
    Find an absolute path within the parent directories of 'test'
    that contains at least one of the target directories of 'dirs'.

    :param test: The full path of a file within the target repository
    :param dirs: The name of the folder we are looking for (ex: .git)
    :param default: Default value to return
    :return: An absolute path of the vcs folder containing the file or the default value
    """
    prev, test = None, os.path.abspath(test)
    while prev != test:
        if any(os.path.isdir(os.path.join(test, d)) for d in dirs):
            return test
        prev, test = test, os.path.abspath(os.path.join(test, os.pardir))
    return default


# Base directory of the working airflow-dags repository install.
# It is used to calculate paths relative to the repository root.
_REPO_BASE_DIR = _find_vcs_root(__file__)


class DagProperties:
    """
    This class helps to override DAG properties dynamically,
    altering them without changing the DAG file in version control.
    To achieve that, it uses Airflow Variables. Variables can be populated
    from Airflow's UI and also from the CLI. And they can be accessed from
    Airflow code (i.e. within a DAG file).

    Usage:
      # Define your properties at the top of your DAG file.
      props = DagProperties(
          some_property="some value",
          another_property=["another", "value"],
          datetime_property=datetime(2023, 1, 1),
      )

      # DagProperties will create an Airflow Variable automatically for you
      # named after the DAG file path (relative to the repository's base path).
      # You can customize it name by passing the parameter _variable_name.
      # The Variable will have the following contents:
      #
      #   {
      #       "some_property": "some value",
      #       "another_property": ["another", "value"],
      #       "datetime_property": "2023-01-01T00:00:00"
      #   }
      #
      # You can modify its contents from the Airflow UI, and DagProperties
      # will catch your changes and update your DAG's properties accordingly.
      # Just remember to wait until your DAG gets parsed again. Check it in the
      # UI under: Details >> DagModel debug information >> last_parsed_time.
      # Note some property types (like datetime) are converted to string,
      # but they are converted back to its object form for the DAG code.
      #
      # WARNING: If the Airflow UI Variable already exists,
      # updates to DagProperties go only in one direction:
      # from Airflow UI -> DAG's properties. If you make changes to
      # DagProperties in the source code, the properties in Airflow UI
      # WILL NOT CHANGE and therefore your source code changes
      # will not take effect.
      #
      # The only way to remedy this is to either manually update the
      # properties in Airflow UI, or delete them from Airflow UI altogether
      # and let this class instantiate an updated version.

      # Access the property values from your DAG like this:
      # Note you should use object notation.
      with DAG(
          start_date=props.datetime_property,
          ...
      ) as dag:
          op = Operator(
              source_table=props.some_property,
              ...
          )
    """

    # The following text is used as the description of
    # any Airflow Variable created using this class.
    VARIABLE_DESCRIPTION_TEMPLATE = """
        This Variable can be used to override the properties of the DAG located at:
        {0}
        To check that your DAG got parsed and updated with the overrides, see:
        Details >> DagModel debug information >> last_parsed_time
        To reset a property to its original value, remove it from the json blob.
    """

    def __init__(self, _variable_name: Optional[str] = None, **kwargs: Any):
        """
        Initializes a DagProperties object.
        Materializes the passed properties into object fields,
        then overrides them with the values from the Airflow Variable,
        and finally updates the Variable if necessary.
        :param _variable_name:
            Name of the Airflow Variable to synchronize properties from/to.
            Default: The filename of the caller DAG (relative to the repo root).
        """
        # Materialize passed properties into object fields.
        self._materialize_fields(kwargs)

        # Override object fields with variable properties.
        variable_name = _variable_name or self._get_variable_name()
        variable_body = self._read_variable(variable_name)
        if variable_body is not None:
            self._override_fields(variable_body)

        # Update variable with new properties.
        new_variable_body = self._encode_fields(list(kwargs.keys()))
        if variable_body is None:
            self._write_variable(variable_name, new_variable_body)
        elif new_variable_body != variable_body:
            self._update_variable(variable_name, new_variable_body)

    def _materialize_fields(self, properties: Dict[str, Any]) -> None:
        """
        Creates and assigns an object field for each of the passed properties.
        """
        for prop_name, prop_value in properties.items():
            setattr(self, prop_name, prop_value)

    def _get_variable_name(self) -> str:
        """
        The Variable name will be the file path of the caller by default.
        Usually this class will be instantiated from a DAG file, so the
        Variable name will end up being the path of the DAG file.
        This is kind of hacky, but it makes this class simpler.
        The path will be relative to the repository base directory.
        """
        # Grab the caller file path from the inspect stack trace.
        caller_file_path: str = inspect.stack()[2][1]
        # Turn path relative to the repository base directory.
        return caller_file_path.removeprefix(os.path.join(str(_REPO_BASE_DIR), ""))

    def _read_variable(self, variable_name: str) -> Dict[str, Any] | None:
        """
        Reads the contents of an Airflow Variable and returns them.
        """
        try:
            variable_body = Variable.get(
                key=variable_name,
                deserialize_json=True,
            )
            return cast(Dict[str, Any], variable_body)
        except KeyError:
            # The Variable does not exist yet.
            return None
        except JSONDecodeError:
            raise ValueError(f"Variable {variable_name} can not be parsed as JSON.")

    def _override_fields(self, variable_body: Dict[str, Any]) -> None:
        """
        Overrides the object fields with the passed properties.
        Raises errors if: 1) There is no field for a given property to override.
        2) The types of the field and the property do not match. 3) The property
        can not be parsed (i.e. a malformed datetime).
        """
        for prop_name, prop_value in variable_body.items():
            # Check that the target field exists.
            if not hasattr(self, prop_name):
                raise KeyError(f"Property {prop_name} is not overridable.")
            # Parse datetime or timedelta string representations into objects.
            field_type = type(getattr(self, prop_name))
            try:
                if field_type == datetime:
                    prop_value = datetime.fromisoformat(prop_value)
                elif field_type == timedelta:
                    prop_value = parse_duration(prop_value)
            except ValueError:
                raise ValueError(f"Property {prop_value} can not be parsed.")
            # Check that the property value matches the field's type.
            if type(prop_value) != field_type:
                raise ValueError(f"Property {prop_name} has an incorrect type.")
            # Override the field with the property value.
            setattr(self, prop_name, prop_value)

    def _encode_fields(self, property_names: List[str]) -> Dict[str, Any]:
        """
        Builds a dictionary with the names and values of the fields matching
        the given property names. When the types of the fields are special
        (datetime, timedelta, etc.), it encodes them to be stored as JSON.
        """
        variable_body = {}
        for prop_name in property_names:
            prop_value = getattr(self, prop_name)
            if type(prop_value) == datetime:
                prop_value = prop_value.isoformat()
            elif type(prop_value) == timedelta:
                prop_value = duration_isoformat(prop_value)
            variable_body[prop_name] = prop_value
        return variable_body

    def _write_variable(
        self,
        variable_name: str,
        variable_body: Dict[str, Any],
    ) -> None:
        """
        Writes an Airflow Variable with the given variable_name and variable_body.
        It adds a handy description.
        """
        description = DagProperties.VARIABLE_DESCRIPTION_TEMPLATE.format(variable_name)
        Variable.set(
            key=variable_name,
            value=variable_body,
            description=description,
            serialize_json=True,
        )

    def _update_variable(
        self,
        variable_name: str,
        variable_body: Dict[str, Any],
    ) -> None:
        """
        Updates an existing Airflow Variable with the given variable_name and variable_body.
        """
        Variable.update(
            key=variable_name,
            value=variable_body,
            serialize_json=True,
        )
