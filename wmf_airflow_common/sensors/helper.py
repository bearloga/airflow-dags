from datetime import datetime, timedelta
from typing import Any, Callable, List

import pendulum
from airflow_client.client.model_utils import (
    ModelNormal,  # base class for Airflow API models
)


def daily_execution_dates_of_month(execution_date: datetime | None = None):
    """
    Return the execution dates of the daily DAG for the month of the given execution date.
    Used in the context of a TaskSensor to wait for the daily DAG to complete for the month of the given execution date.

    :param context: The context for the task_instance of interest.
    :return: A list of execution dates of the daily DAG for the month of the given execution date.
    """
    if execution_date is None:
        return []
    month_start = execution_date.replace(day=1)
    month_end = month_start + timedelta(days=32)
    month_end = month_end.replace(day=1) - timedelta(days=1)
    return [month_start + timedelta(days=d) for d in range(0, (month_end - month_start).days + 1)]


def get_daily_execution_dates_fn(num_days: int = 1) -> Callable[[datetime, Any], list[datetime]]:
    """Return a function that generates the desired number of execution dates
    when provided with a starting execution date and the Airflow Context object.

    The list of generated execution dates starts with the execution date
    and goes forward in time.

    :param num_days: Number of days in the list the returned function will generate
    """

    def _get_dates(exec_date: datetime | None = None, context=None) -> list[datetime]:
        if exec_date is None:
            return []
        return [
            (exec_date + timedelta(days=i)).replace(hour=0, minute=0, second=0, microsecond=0) for i in range(num_days)
        ]

    return _get_dates


def filter_api_response(
    key_fn: Callable[[Any], Any],
    vals: List[Any],
    api_response: ModelNormal,
):
    """Helper function to filter Airflow's REST API responses for a given key,
    for a given set of expected values.
    """

    if not vals:
        # If no expected values are given, do not filter at all.
        return api_response

    new_data = []
    for val in vals:
        for rsp in api_response:
            rsp_val = key_fn(rsp)
            if isinstance(rsp_val, str) and isinstance(val, datetime):
                # Sometimes, the API gives datetimes as strings
                rsp_val = pendulum.parse(rsp_val)
            if rsp_val == val:
                new_data.append(rsp)

    return new_data
