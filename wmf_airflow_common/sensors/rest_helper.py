import logging
from datetime import datetime
from typing import List, Tuple

from airflow.exceptions import AirflowException
from airflow_client import client
from airflow_client.client.api import dag_api, dag_run_api, task_instance_api
from airflow_client.client.model.dag import DAG
from airflow_client.client.model.dag_run import DAGRun
from airflow_client.client.model.task import Task
from airflow_client.client.model.task_instance import TaskInstance

from wmf_airflow_common.sensors.helper import filter_api_response

logger = logging.getLogger(__name__)


def _get_dag(config: client.Configuration, dag_id: str) -> DAG:
    """Retrieves DAG information for a given DAG ID
    available at the configured Airflow instance
    """
    dag_info: DAG | None = None

    with client.ApiClient(config) as api_client:
        api_instance = dag_api.DAGApi(api_client)

        try:
            dag_info = api_instance.get_dag(dag_id)
        except client.ApiException as e:
            logger.error("Exception when calling DAGApi->get_dag: %s", e)
            raise AirflowException(f"DAGApi->get_dag exception: {e}") from e

    return dag_info


def _get_groups(task_ids: List[str]) -> List[str]:
    groups = set()

    for task_id in task_ids:
        sp = task_id.split(".")
        if len(sp) > 1:
            groups.add(sp[0])

    return list(groups)


def _get_task_ids(config: client.Configuration, dag_id: str) -> List[str]:
    """Retrieves a list of task IDs for a given DAG ID
    available at the configured Airflow instance
    """
    task_ids: List[str] = []

    with client.ApiClient(config) as api_client:
        api_instance = dag_api.DAGApi(api_client)

        try:
            api_response: List[Task] = api_instance.get_tasks(dag_id)
            task_ids.extend([x.task_id for x in api_response.tasks])
        except client.ApiException as e:
            logger.error("Exception when calling DAGApi->get_tasks: %s", e)
            raise AirflowException(f"DAGApi->get_tasks exception: {e}") from e

    return task_ids


def _get_dag_runs(
    config: client.Configuration, dag_id: str, states: List[str], dttm_filter: List[datetime]
) -> List[DAGRun]:
    """Retrieves a list of DAG runs filtered for given states and execution dates"""

    dag_runs = []

    execution_date_gte = None
    if dttm_filter:
        execution_date_gte = sorted(dttm_filter)[-1]
        execution_date_gte = execution_date_gte.subtract(days=3)

    with client.ApiClient(config) as api_client:
        # The name of the field to order the results by.
        # Prefix a field name with `-` to reverse the sort order.
        order_by = "-execution_date"

        api_instance = dag_run_api.DAGRunApi(api_client)
        try:
            if execution_date_gte:
                api_response = api_instance.get_dag_runs(
                    dag_id,
                    limit=10,
                    execution_date_gte=execution_date_gte,
                    order_by=order_by,
                )
            else:
                api_response = api_instance.get_dag_runs(
                    dag_id,
                    limit=10,
                    order_by=order_by,
                )

            dag_runs = api_response.dag_runs
            dag_runs = filter_api_response(lambda x: x.state.value, states, dag_runs)
            dag_runs = filter_api_response(lambda x: x.execution_date, dttm_filter, dag_runs)

        except client.ApiException as e:
            logger.error("Exception when calling DAGRunApi->get_dag_runs: %s", e)
            raise AirflowException(f"DAGRunApi->get_dag_runs exception: {e}") from e

    return dag_runs


def _get_task_instances(
    config: client.Configuration,
    external_dag_id: str,
    external_task_ids: List[str],
    states: List[str],
    dttm_filter: List[datetime],
) -> List[TaskInstance]:
    """Get a list of TaskInstance objects representing task instances
    for given task IDs, filtered for given states and dttm values.
    """
    filt_dag_runs = _get_dag_runs(config, external_dag_id, states, dttm_filter)

    res_task_instances = []

    with client.ApiClient(config) as api_client:
        for dag_run in filt_dag_runs:
            api_instance = task_instance_api.TaskInstanceApi(api_client)
            try:
                # List task instances
                api_response = api_instance.get_task_instances(external_dag_id, dag_run.dag_run_id)

                task_instances = api_response.task_instances

                task_instances = filter_api_response(lambda x: x.task_id, external_task_ids, task_instances)
                task_instances = filter_api_response(lambda x: x.state.value, states, task_instances)
                task_instances = filter_api_response(lambda x: x.execution_date, dttm_filter, task_instances)

                res_task_instances.extend(task_instances)
            except client.ApiException as e:
                logger.error("Exception when calling TaskInstanceApi->get_task_instances: %s", e)
                raise AirflowException(f"TaskInstanceApi->get_task_instances exception: {e}") from e

    return res_task_instances


def _get_count(
    config: client.Configuration,
    dttm_filter: List[datetime],
    external_task_ids: List[str],
    external_task_group_id: str,
    external_dag_id: str,
    states: List[str],
) -> int:
    """Get the count of records against dttm filter and states.

    :param dttm_filter: date time filter for execution date
    :param external_task_ids: The list of task_ids
    :param external_task_group_id: The ID of the external task group
    :param external_dag_id: The ID of the external DAG.
    :param states: task or dag states
    """
    if not dttm_filter:
        return 0

    if external_task_ids:
        task_instances = _get_task_instances(config, external_dag_id, external_task_ids, states, dttm_filter)

        count = len(task_instances) / len(external_task_ids)

    elif external_task_group_id:
        external_task_group_task_instances = _get_external_task_group_task_instances(
            config,
            dttm_filter,
            external_task_group_id,
            external_dag_id,
        )

        if not external_task_group_task_instances:
            count = 0
        else:
            state_filtered_task_instances = filter_api_response(
                lambda x: x.state.value, states, external_task_group_task_instances
            )

            count = len(state_filtered_task_instances) / len(external_task_group_task_instances)
    else:
        dag_runs = _get_dag_runs(config, external_dag_id, states, dttm_filter)
        count = len(dag_runs)

    return int(count)


def _get_external_task_group_task_instances(
    config: client.Configuration,
    dttm_filter: List[datetime],
    external_task_group_id: str,
    external_dag_id: str,
) -> List[TaskInstance]:
    """Get the list of TaskInstance objects in a given
    task group, filtered for datetime.

    :param dttm_filter: date time filter for execution date
    :param external_task_group_id: The ID of the external task group
    :param external_dag_id: The ID of the external DAG.
    """
    external_task_group_task_ids = _get_task_ids(config, external_dag_id)
    external_task_group_task_ids = list(
        filter(lambda task_id: task_id.startswith(external_task_group_id), external_task_group_task_ids)
    )

    if not external_task_group_task_ids:
        return []

    states: List[str] = []

    task_instances = _get_task_instances(config, external_dag_id, external_task_group_task_ids, states, dttm_filter)

    return task_instances


def _get_external_task_group_task_ids(
    config: client.Configuration,
    dttm_filter: List[datetime],
    external_task_group_id: str,
    external_dag_id: str,
) -> List[Tuple[str, int]]:
    """Get the list of (task_id, map_index) tuples of task instances in a given
    task group, filtered for datetime.

    :param dttm_filter: date time filter for execution date
    :param external_task_group_id: The ID of the external task group
    :param external_dag_id: The ID of the external DAG.
    """
    task_instances = _get_external_task_group_task_instances(
        config,
        dttm_filter,
        external_task_group_id,
        external_dag_id,
    )

    return [(task["task_id"], task["map_index"]) for task in task_instances]
