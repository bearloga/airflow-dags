import os
from pathlib import Path
from typing import Any, Optional
from urllib.parse import urlparse

import airflow
from airflow.exceptions import AirflowConfigException
from pyarrow.fs import FileSelector, HadoopFileSystem

SPARK_PART_FILE_BEGINNING = "part-"


def hdfs_client(name_node: str) -> HadoopFileSystem:
    """Returns an HDFS client for the given Hadoop name node.
    Args:
        name_node   URI for Hadoop name node.
                    (i.e.: hdfs://analytics-hadoop)
    """
    stream = os.popen("hdfs classpath --glob")
    os.environ["CLASSPATH"] = stream.read().strip()
    return HadoopFileSystem(name_node)


def get_first_part_in_spark_output_dir(hdfs_client: HadoopFileSystem, spark_output_dir: Optional[str]) -> Optional[str]:
    """
    Fetch the first data file path from a spark-sql `INSERT OVERWRITE DIRECTORY` output.
    ex:
      - what's in your Spark output directory:
        /user/me/test/_SUCCESS
        /user/me/test/part-00000-d60c80c5-ebe0-4b73-8c47-1fd896ab403b-c000.csv
        /user/me/test/part-00001-d60c80c5-ebe0-4b73-8c47-1fd896ab403b-c000.csv
      - what you will get:
        /user/me/test/part-00000-d60c80c5-ebe0-4b73-8c47-1fd896ab403b-c000.csv
    :param hdfs_client: a Pyarrow HDFS client
    :param spark_output_dir: The HDFS directory where Spark wrote the output
    :return: An optional HDFS path
    """
    selector = FileSelector(spark_output_dir, recursive=True)
    files = hdfs_client.get_file_info(selector)
    part_files = [f.path for f in files if f"{spark_output_dir}/{SPARK_PART_FILE_BEGINNING}" in f.path]
    return next(iter(part_files), None)


def airflow_config_get(section: str, key: str) -> Optional[str]:
    """
    Wraps airflow.configuration.conf.get catching
    AirflowConfigException if config isn't set, and
    returns None instead.

    :param section:
        Airflow config section

    :param key:
        Airflow config key

    """
    try:
        return str(airflow.configuration.conf.get(section, key))
    except AirflowConfigException:
        return None


def wmf_airflow_instance_name() -> Optional[str]:
    """
    Returns the AIRFLOW_INSTANCE_NAME env var, or None.
    AIRFLOW_INSTANCE_NAME is set in WMF Airflow instances.
    """
    # AIRFLOW_INSTANCE_NAME is set in official WMF airflow instances, but not development instances.
    # See AIRFLOW_HOME/bin/airflow-*-profile.sh managed by Puppet.
    return os.environ.get("AIRFLOW_INSTANCE_NAME")


def airflow_environment_name() -> Optional[str]:
    """
    Returns the current Airflow environment name.
    This is either the value of the AIRFLOW_ENVIRONMENT_NAME env var,
    or 'wmf' if AIRFLOW_INSTANCE_NAME is set, or None.
    """
    env_name = os.environ.get("AIRFLOW_ENVIRONMENT_NAME")
    # special case: return 'wmf' if AIRFLOW_INSTANCE_NAME is set.
    if env_name is None and wmf_airflow_instance_name() is not None:
        env_name = "wmf"
    return env_name


def is_wmf_airflow_instance() -> bool:
    """
    Returns true if this is a 'WMF' airflow instance.

    For now this is always true if airflow_environment_name()
    starts with 'wmf'.
    """
    env_name = airflow_environment_name()
    return env_name is not None and env_name.startswith("wmf")


def is_relative_uri(uri: str) -> bool:
    """
    Returns false if the url starts with a / or a protocol scheme,
    else true.

    :param uri: URI to check for relativeness.
    :return: True if the uri is relative
    """
    return not (Path(uri).is_absolute() or bool(urlparse(uri).scheme))


def resolve_kwargs_default_args(kwargs: dict, key: str) -> Any:
    """
    Returns the value found in kwargs for key if it exists, or the value
    found in kwargs['default_args'], or None.
    """
    return kwargs.get(key, kwargs.get("default_args", {}).get(key))


def dict_add_or_append_string_value(conf: dict, key: str, value: str, sep: str = " ") -> None:
    """
    Given a dict[str,str], add the key value if not present, or append the value
    using the provided separator if the key is present
    """
    if key in conf:
        conf[key] = conf[key] + sep + value
    else:
        conf[key] = value


def sanitize_string(input_string: str, invalid_chars: str = ":+", new: str = "_") -> str:
    """
    Replace invalid characters from input_string.
    A helper function used to sanitize string e.g. Airflow's run_id
    to be used as identifier to a filename compatible with HDFS.
    """
    sanitized_string = input_string
    for char in invalid_chars:
        sanitized_string = sanitized_string.replace(char, new)

    return sanitized_string


def one_line(s: str):
    """
    Takes a string with potentially multiple lines and multiple spaces and convert to one space and one line.
    Useful to make SQL statements more readable in test fixtures
    """
    return " ".join(s.split())


def convert_dict_to_list(d: dict) -> list:
    """
    Convert a dictionary to a list of key-value pairs.
    """
    return [item for pair in d.items() for item in pair]
