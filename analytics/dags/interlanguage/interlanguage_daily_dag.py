"""
# This job waits for the presence of all pageview partions in a day then
# aggregates cross-wiki navigation counts

This job aggregates pageview_actor records into counts of navigation by users
from one wiki project to another. Only counting desktop site browsing.

The resulting output is;
* partitioned by year, month, day
* located in /wmf/data/wmf/interlanguage/navigation/daily
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    dag_start_date=datetime(2024, 1, 25),
    # SLA and alert email
    dag_sla=timedelta(hours=6),
    alerts_email=alerts_email,
    # HQL locations
    hql_hive=f"{hql_directory}/interlanguage/daily/interlanguage_navigation.hql",
    hql_iceberg=f"{hql_directory}/interlanguage/daily/interlanguage_navigation_iceberg.hql",
    source_table="wmf.pageview_actor",
    # destination tables
    destination_table="wmf.interlanguage_navigation",
    destination_table_iceberg="wmf_traffic.interlanguage_navigation",
)


# Instantiate DAG
with create_easy_dag(
    dag_id="interlanguage_daily",
    description="write_interlanguage_data_to_hive",
    doc_md=__doc__,
    start_date=props.dag_start_date,
    schedule="@daily",
    tags=["daily", "from_hive", "to_hive", "uses_hql", "requires_wmf_pageview_actor"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    pageview_actor_sensor = dataset("hive_wmf_pageview_actor").get_sensor_for(dag)

    run_interlanguage_navigation_hive = SparkSqlOperator(
        task_id="filter_pageview_actor",
        sql=props.hql_hive,
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "refinery_hive_jar_path": artifact("refinery-hive-0.2.1-shaded.jar"),
            "padded_date": "{{ ds }}",
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "coalesce_partitions": 1,
        },
        conf={"spark.dynamicAllocation.maxExecutors": 32},
    )

    run_interlanguage_navigation_iceberg = SparkSqlOperator(
        task_id="filter_pageview_actor_iceberg",
        sql=props.hql_iceberg,
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table_iceberg,
            "refinery_hive_jar_path": artifact("refinery-hive-0.2.1-shaded.jar"),
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "coalesce_partitions": 1,
        },
        conf={"spark.dynamicAllocation.maxExecutors": 32},
    )

    pageview_actor_sensor >> run_interlanguage_navigation_hive >> run_interlanguage_navigation_iceberg
