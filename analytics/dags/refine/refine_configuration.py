"""
This module contains the RefineDatasetConfiguration class that is used to extract the configuration from the
event-stream-config configuration and provide a set of methods to generate the configuration needed for the refine
job.

Example of usage:
    configuration = RefineDatasetConfiguration(configuration_from_event_stream_config)
"""
import re
from typing import Any, Dict, List


class RefineConfiguration:
    SPARK_JOB_SCALE_PROFILES: Dict[str, Dict[str, str | int]] = {
        # When in local mode, use a single executor providing a clue to Spark about parallelism.
        # Without specifying the number of executors, Spark will use the number of cores available on the machine.
        "small": {"master": "local[1]", "driver_cores": 2, "driver_memory": "4G"},
        "medium": {
            "master": "yarn",
            "max_executors": 4,
            "driver_cores": 1,
            "driver_memory": "4G",
            "executor_cores": 4,
            "executor_memory": "8G",
        },
        "large": {
            "master": "yarn",
            "max_executors": 32,
            "driver_cores": 2,
            "driver_memory": "4G",
            "executor_cores": 4,
            "executor_memory": "8G",
        },
    }
    """
    SPARK_JOB_SCALE_PROFILES is a dictionary that defines various scaling profiles for
    running Spark jobs, specifying the configurations for small, medium, and large jobs.

    It describes the scale of the Refine job in charge of importing data from Kafka to HDFS. Depending on the traffic
    on the stream, the job can be configured to run within a Skein container or on a YARN cluster. The part of the
    Refine job in charge of evolving the tables will always run in local mode within a Skein container.

    Each profile includes the following attributes
    - "master": Specifies the cluster manager for the Spark application (e.g., "local" or "yarn"). Local is used within
      the Skein container
    - "driver_cores": Number of cores assigned to the driver process.
    - "driver_memory": Amount of memory allocated to the driver.
    - "max_executors" (only for "yarn" master): Maximum number of executors that can be used by the job.
    - "executor_cores" (only for "yarn" master): Number of cores per executor process.
    - "executor_memory" (only for "yarn" master): Amount of memory allocated to each executor.

    Profiles:
    - "small": Suitable to launch in local mode within the Skein container.
    - "medium": Suitable for moderate workloads with up to 4 executors on a YARN cluster.
    - "large": Suitable for large workloads with up to 32 executors on a YARN cluster.
    """

    DEFAULT_HIVE_PARTITIONS = {"year": "LONG", "month": "LONG", "day": "LONG", "hour": "LONG"}
    """
    DEFAULT_HIVE_PARTITIONS is a dictionary that defines the default partitioning scheme
    for Hive tables, where each key represents a time-based partition column
    and the corresponding value specifies the data type.
    """

    TRANSFORM_FUNCTIONS_BASE = [
        "org.wikimedia.analytics.refinery.job.refine.remove_canary_events",
        "org.wikimedia.analytics.refinery.job.refine.deduplicate",
        "org.wikimedia.analytics.refinery.job.refine.geocode_ip",
        "org.wikimedia.analytics.refinery.job.refine.parse_user_agent",
        "org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain",
        "org.wikimedia.analytics.refinery.job.refine.add_normalized_host",
        "org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",
    ]
    """
    TRANSFORM_FUNCTIONS_BASE is a list of transformation functions applied during the
    data refinement process in the Wikimedia Analytics Refinery.

    Each function in the list represents a step in the data pipeline, responsible for
    modifying or enriching the data as follows:

    - "remove_canary_events": Filters out canary events from the data stream.
    - "deduplicate": Removes duplicate records from the dataset.
    - "geocode_ip": Enriches data by mapping IP addresses to geographical locations.
    - "parse_user_agent": Parses user-agent strings into structured information (e.g., browser, OS).
    - "add_is_wmf_domain": Adds a flag indicating if the domain is associated with the Wikimedia Foundation.
    - "add_normalized_host": Normalizes the host information in the data.
    - "normalizeFieldNamesAndWidenTypes": Normalizes field names and adjusts field types for consistency.

    This list represents the core set of transformations applied to ensure data quality,
    consistency, and enrichment before further processing or storage.
    """

    def __init__(self, stream_config_entry):
        """
        Extract the configuration from the event-stream-config configuration.

        Example of configuration:
            "schema_title": "mediawiki/user/blocks-change",
            "destination_event_service": "eventgate-main",
            "topic_prefixes": [
                "eqiad.",
                "codfw."
            ],
            "canary_events_enabled": true,
            "consumers": {
                "analytics_hadoop_ingestion": {
                    "job_name": "event_default",
                    "enabled": true
                },
                "analytics_hive_ingestion": {
                    "enabled": true
                }
            },
            "stream": "mediawiki.user-blocks-change",
            "topics": [
                "eqiad.mediawiki.user-blocks-change",
                "codfw.mediawiki.user-blocks-change"
            ]
        :param stream_config_entry:
        """
        self.stream = stream_config_entry["stream"]
        self.schema_title = stream_config_entry["schema_title"]
        self.topic_prefixes = stream_config_entry.get("topic_prefixes", [])
        self.consumers = stream_config_entry.get("consumers", {})
        self.topics = stream_config_entry.get("topics", [])
        self.analytics_hadoop_ingestion_enabled = self.consumers.get("analytics_hadoop_ingestion", {}).get(
            "enabled", False
        )
        analytics_hive_ingestion = self.consumers.get("analytics_hive_ingestion", {})
        self.analytics_hive_ingestion_enabled = analytics_hive_ingestion.get("enabled", False)
        self.analytics_hive_ingestion_scale = analytics_hive_ingestion.get("spark_job_ingestion_scale", "small")
        # Extras:
        self.destination_event_service = stream_config_entry.get("destination_event_service", "")
        self.canary_events_enabled = stream_config_entry["canary_events_enabled"]

    def _spark_job_scale_profile(self) -> Dict[str, int | str]:
        """Return the Spark refine job scale parameters."""
        return self.SPARK_JOB_SCALE_PROFILES.get(self.analytics_hive_ingestion_scale, {})

    def _hive_partition_columns(self) -> Dict[str, str]:
        """Return the partition columns for the Hive table. e.g. datacenter,year,month,day,hour"""
        if self._is_eventlogging_legacy():
            return self.DEFAULT_HIVE_PARTITIONS
        return {**{"datacenter": "STRING"}, **self.DEFAULT_HIVE_PARTITIONS}

    def _is_eventlogging_legacy(self):
        return "eventlogging_" in self.stream

    def _hdfs_source_paths(self, gobblin_partition_ending: str) -> List[str]:
        """
        Get the HDFS source paths.
        example:
            [
                "/wmf/data/raw/event/eqiad.ios.notification_interaction/year=2021/month=01/day=01/hour=00",
                "/wmf/data/raw/event/codfw.ios.notification_interaction"
            ],
        :return:
        """
        if self._is_eventlogging_legacy():
            return [f"/wmf/data/raw/eventlogging_legacy/{self.stream}/{gobblin_partition_ending}"]
        else:
            return [
                f"/wmf/data/raw/event/{prefix}{self.stream}/{gobblin_partition_ending}"
                for prefix in self.topic_prefixes
            ]

    def _hive_partition_paths(self, hive_partition_ending: str) -> List[str]:
        """
        Get the Hive partition paths.
        example:
            [
                "datacenter=eqiad/year=2021/month=01/day=01/hour=00",
                "datacenter=codfw/year=2021/month=01/day=01/hour=00"
            ],
        :return:
        """
        return [f"{self._get_dc_part_from_kafka_topic(topic)}{hive_partition_ending}" for topic in self.topics]

    def _get_dc_part_from_kafka_topic(self, topic: str) -> str:
        """Extract the datacenter Hive partition from a Kafka topic.
        e.g. 'eqiad' from 'eqiad.mediawiki.user-blocks-change'"""
        if self._is_eventlogging_legacy():
            return ""
        return f"datacenter={topic.split('.')[0]}/"

    def _replace_forbidden_characters(self, string: str) -> str:
        """Replace forbidden characters in a string. All non-alphanumeric characters are replaced by an underscore."""
        return re.sub(r"[^a-zA-Z0-9]", "_", string)

    def _hive_table_name_from_stream_name(self) -> str:
        """Return the stream name usable for table name."""
        stream = self.stream
        if self._is_eventlogging_legacy():
            stream = self.stream.split("_")[1]  # Remove eventlogging_ prefix
        return self._replace_forbidden_characters(stream)

    def _table_location(self, table_base_location: str = "hdfs:///wmf/data/event/") -> str:
        """
        Get the table location on hdfs
        example:
            hdfs://analytics-hadoop/wmf/data/event/VirtualPageView
            hdfs://analytics-hadoop/wmf/data/event/ios_search
        :return:
        """
        return f"{table_base_location}/{self._hive_table_name_from_stream_name()}"

    def _transform_functions(self) -> List[str]:
        if self._is_eventlogging_legacy():
            return [
                "org.wikimedia.analytics.refinery.job.refine.filter_allowed_domains"
            ] + self.__class__.TRANSFORM_FUNCTIONS_BASE
        return self.__class__.TRANSFORM_FUNCTIONS_BASE

    def _hive_pre_partitions(self) -> List[str]:
        """
        :return: List of Hive pre-partitions. e.g. ['datacenter=eqiad', 'datacenter=codfw']
        """
        if self._is_eventlogging_legacy():
            return []
        return [f"datacenter={topic_prefix.split('.')[0]}" for topic_prefix in self.topic_prefixes]

    def _common_dict(
        self,
        gobblin_partition_ending: str,
        table_base_location: str,
        diff_database: str,
    ) -> Dict[str, Any]:
        return {
            "stream": self.stream,
            "schema_uri": f"/{self.schema_title}/latest",
            "hdfs_source_paths": self._hdfs_source_paths(gobblin_partition_ending),
            "table_location": self._table_location(table_base_location),
            "transform_functions": self._transform_functions(),
            "spark_refine_job_scale_params": self._spark_job_scale_profile(),
            "spark_refine_job_scale": self.analytics_hive_ingestion_scale,
            "hive_pre_partitions": self._hive_pre_partitions(),
            "diff_hive_table": f"{diff_database}.{self._hive_table_name_from_stream_name()}",
        }

    def hive_dict(
        self,
        hive_partition_ending: str,
        gobblin_partition_ending: str,
        database: str = "event",
        table_base_location: str = "hdfs:///wmf/data/event/",
        diff_database: str = "event",
    ) -> Dict[str, Any]:
        return {
            "table_format": "hive",
            "hive_table": f"{database}.{self._hive_table_name_from_stream_name()}",
            "hive_partition_columns": self._hive_partition_columns(),
            "hive_partition_paths": self._hive_partition_paths(hive_partition_ending),
            **self._common_dict(gobblin_partition_ending, table_base_location, diff_database),
        }
