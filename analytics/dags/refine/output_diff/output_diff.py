"""
Diff two datasets. Returns non 0 in case of differences.
"""
import argparse
import datetime
import os
import sys
from typing import Dict

# Needed to import the diff lib available locally.
sys.path.append(os.getcwd())
# Next line is also adding the diff method on Dataframe
from gresearch.spark.diff import DiffOptions  # noqa: E402
from pyspark.sql import DataFrame, SparkSession  # noqa: E402
from pyspark.sql.functions import col, expr, struct  # noqa: E402
from pyspark.sql.types import ArrayType, MapType, StructType  # noqa: E402


def parse_arguments() -> argparse.Namespace:
    """
    Parse command line arguments.
    :return: Parsed arguments
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--legacy_table", type=str, required=True, help="Legacy table name")
    parser.add_argument("--new_table", type=str, required=True, help="New table name")
    parser.add_argument("--new_table_format", type=str, required=True, help="Hive or Iceberg")
    parser.add_argument("--year", type=int, required=True, help="Year")
    parser.add_argument("--month", type=int, required=True, help="Month")
    parser.add_argument("--day", type=int, required=True, help="Day")
    parser.add_argument("--hour", type=int, required=True, help="Hour")
    return parser.parse_args()


def rearrange_schema(schema: StructType) -> StructType:
    """Recursively sort fields in the schema."""

    def sort_struct_fields(struct_type: StructType) -> StructType:
        sorted_fields = sorted(struct_type.fields, key=lambda f: f.name)
        sorted_schema = StructType()
        for field in sorted_fields:
            if isinstance(field.dataType, StructType):
                sorted_schema.add(field.name, sort_struct_fields(field.dataType), field.nullable)
            elif isinstance(field.dataType, ArrayType) and isinstance(field.dataType.elementType, StructType):
                sorted_schema.add(field.name, ArrayType(sort_struct_fields(field.dataType.elementType)), field.nullable)
            elif isinstance(field.dataType, MapType) and isinstance(field.dataType.valueType, StructType):
                sorted_schema.add(
                    field.name,
                    MapType(field.dataType.keyType, sort_struct_fields(field.dataType.valueType)),
                    field.nullable,
                )
            else:
                sorted_schema.add(field.name, field.dataType, field.nullable)
        return sorted_schema

    return sort_struct_fields(schema)


def rearrange_columns(df: DataFrame) -> DataFrame:
    """Rearrange columns in a DataFrame according to the sorted schema."""
    sorted_schema = rearrange_schema(df.schema)

    def rearrange_expr(schema: StructType, prefix="") -> list:
        expressions = []
        for field in schema.fields:
            if field.name == "_schema":  # Skip _schema field as it was not filled in the legacy Refine
                continue
            field_name = f"{prefix}.{field.name}" if prefix else field.name
            if isinstance(field.dataType, StructType):
                expressions.append(struct(*rearrange_expr(field.dataType, field_name)).alias(field.name))
            elif isinstance(field.dataType, ArrayType) and isinstance(field.dataType.elementType, StructType):
                expressions.append(expr(f"to_json({field_name})").alias(field.name))
            elif isinstance(field.dataType, MapType):
                expressions.append(expr(f"to_json({field_name})").alias(field.name))
            else:
                expressions.append(col(field_name).alias(field.name))
        return expressions

    sorted_columns = rearrange_expr(sorted_schema)
    return df.select(*sorted_columns)


def reformat(df: DataFrame, new_table_format="hive") -> DataFrame:
    """
    Reformat the DataFrame to be used in diff function.
    :param df: Input DataFrame
    :param new_table_format: Format of the new table (hive or iceberg)
    :return: Reformatted DataFrame
    """
    # _schema used to be not filled in the old version.
    df = df.drop("_schema").orderBy(*[c.asc() for c in _get_ordering_columns(df)])

    if "original_event_info" in df.schema.fieldNames():
        # drop _schema in the original_event_info struct
        df = df.withColumn(
            "original_event_info",
            struct(
                *[
                    col("original_event_info").getItem(f).drop("_schema").alias(f)
                    for f in df.schema["original_event_info"].dataType.fieldNames()
                ]
            ),
        )

    if new_table_format == "iceberg":
        df = (
            df.withColumn("year", col("year(meta.dt) as year"))
            .withColumn("month", col("month(meta.dt) as month"))
            .withColumn("day", col("day(meta.dt) as day"))
            .withColumn("hour", col("hour(meta.dt) as hour"))
        )
    # We rearrange the columns by reordering the fields alphabetically in the schema.
    # If not, gresearch.spark.diff will treat the schema as different.
    return rearrange_columns(df)


def _get_ordering_columns(df):
    ordering_columns = [
        col("meta.dt"),
        col("meta.id"),
        col("meta.request_id"),
    ]
    meta_field: StructType = df.schema["meta"].dataType
    if "uuid" in meta_field.fieldNames():
        ordering_columns.append(col("meta.uuid"))
    return ordering_columns


def get_partition_df(
    table: str, spark: SparkSession, year: str, month: str, day: str, hour: str, new_table_format: str = "hive"
) -> DataFrame:
    if new_table_format == "iceberg":
        data_interval_start = datetime.datetime(int(year), int(month), int(day), int(hour), 0)
        data_interval_end = data_interval_start + datetime.timedelta(hours=1)
        partition_condition = (
            f"meta.dt >= '{data_interval_start.to_iso8601_string()}' "
            f"and meta.dt < '{data_interval_end.to_iso8601_string()}'"
        )
    else:  # Hive table
        partition_condition = f"year = {year} and month = {month} and day = {day} and hour = {hour}"
    return reformat(spark.sql(f"select * from {table} where {partition_condition}"), new_table_format)


DIFF_COLUMN_VALUES = {"N": "Rows without difference", "I": "Rows inserted", "C": "Rows changed", "D": "Rows deleted"}


def main():
    args = parse_arguments()
    spark = SparkSession.getActiveSession()
    if spark is None:
        spark = SparkSession.builder.appName("OutputDiff").enableHiveSupport().getOrCreate()
    log4jLogger = spark.sparkContext._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("output_diff")
    logger.info("Starting the diff process.")
    partition_name = f"{args.year}/{args.month}/{args.day}-{args.hour}:00"
    logger.info(f"Partition: {partition_name}")
    new_table_df: DataFrame = get_partition_df(
        args.new_table, spark, args.year, args.month, args.day, args.hour
    ).cache()
    new_table_df.printSchema()
    legacy_table_df: DataFrame = get_partition_df(
        args.legacy_table, spark, args.year, args.month, args.day, args.hour
    ).cache()
    legacy_table_df.printSchema()
    legacy_table_df_count = legacy_table_df.count()
    logger.info(f"Legacy Table: {args.legacy_table} {partition_name} {legacy_table_df_count} row(s)")
    logger.info(f"New Table: {args.new_table} {partition_name} {new_table_df.count()} row(s)")
    diff_counts: Dict[str, int] = {
        row["diff"]: row["count"] for row in new_table_df.diff(legacy_table_df).groupBy("diff").count().collect()
    }
    logger.info(f"Counts: {diff_counts}")
    if diff_counts.get("N", 0) != legacy_table_df_count or diff_counts.get("I", 0) + diff_counts.get("D", 0) != 0:
        diff_options = DiffOptions().with_change_column("changes")
        diff_with_changes = new_table_df.diff_with_options(legacy_table_df, diff_options, "meta_id").cache()
        logger.info("There are differences between the two datasets. Examples:")
        logger.info("Inserted rows:")
        diff_with_changes.where(col("diff") == "I").show(10, False)
        logger.info("Deleted rows:")
        diff_with_changes.where(col("diff") == "D").show(10, False)
        changed_columns = diff_with_changes.where(col("diff") == "C").select("changes").limit(1).collect()
        if len(changed_columns) > 0:
            logger.info("Changed rows examples:")
            changed_columns = [(f"left_{column}", f"right_{column}") for column in changed_columns[0]["changes"]]
            changed_columns = [item for sublist in changed_columns for item in sublist]  # flattenization
            diff_with_changes.where(col("diff") == "C").select("diff", "changes", *changed_columns).show(10, False)
        diff_with_changes.unpersist()
        exit(1)
    else:
        logger.info("The two datasets are identical.")
        exit(0)


if __name__ == "__main__":
    main()
