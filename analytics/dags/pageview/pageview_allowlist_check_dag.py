"""
### Checks a day of pageview data for pageviews to unexpected domains
* Sends an alert email if unexpected values are found

Ouput
* Output is appended into (year, month, day, hour) partitions in /wmf/data/wmf/pageview/hourly,
* and then archived into /wmf/data/archive/pageview/legacy/hourly

Variables for Testing
{
    "pageview_allowlist_check_config":
    {
        "start_date": "2023-02-20",
        "hql_path": "/home/milimetric/refinery/hql/pageview_allowlist/pageview_allowlist_check.hql",
        "allowlist_table": "milimetric.pageview_allowlist",
        "unexpected_values_table": "milimetric.pageview_unexpected_values",
        "unexpected_email": "milimetric@wikimedia.org",
        "unexpected_hdfs_path": "/user/milimetric/wmf/data/wmf/pageview/unexpected_values",
        "default_args": {
            "owner": "analytics-privatedata",
            "email": "milimetric@wikimedia.org"
        }
    }
}
"""

from datetime import datetime, timedelta
from typing import Any, List

from airflow import DAG
from airflow.operators.email import EmailOperator
from airflow.operators.python import BranchPythonOperator
from pyarrow.fs import FileSelector

from analytics.config.dag_config import (
    alerts_email,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common import util
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "pageview_allowlist_check"
var_props = VariableProperties(f"{dag_id}_config")
# for this check, projectview has all the dimensions we need and is much more efficient to search
source_table = "wmf.projectview_hourly"
hql_path = var_props.get("hql_path", f"{hql_directory}/pageview_allowlist/pageview_allowlist_check.hql")
allowlist_table = var_props.get("allowlist_table", "wmf.pageview_allowlist")
unexpected_values_table = var_props.get("unexpected_values_table", "wmf.pageview_unexpected_values")
unexpected_email = var_props.get("unexpected_email", alerts_email)

start_date = var_props.get_datetime("start_date", datetime(2023, 2, 28))
year = "{{data_interval_start.year}}"
month = "{{data_interval_start.month}}"
day = "{{data_interval_start.day}}"
unexpected_hdfs_path = var_props.get("unexpected_hdfs_path", "/wmf/data/wmf/pageview/unexpected_values") + (
    "/year={{data_interval_start.year}}" + "/month={{data_interval_start.month}}" + "/day={{data_interval_start.day}}/"
)


# If path contains a file with size > 0, it returns the first such file,
# otherwise, None
def get_unexpected_file(path):
    hdfs = util.hdfs_client(hadoop_name_node)

    potential_file_list = hdfs.get_file_info(FileSelector(path))
    return next(filter(lambda f: f and f.size and f.size > 0, potential_file_list), None)


def check_for_unexpected_values(**kwargs: Any) -> List[str]:
    """
    Determines whether there were pageviews to a domain not in the allow list.
    """
    file_with_unexpected = get_unexpected_file(kwargs["unexpected_values_path"])

    if file_with_unexpected:
        # Return the task_id of the next task to run.
        return ["send_unexpected_email"]

    # Otherwise, skip downstream tasks.
    return []


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=start_date,
    schedule="@daily",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
    tags=["daily", "from_hive", "to_hive", "uses_email", "requires_wmf_projectview_hourly"],
) as dag:
    sense_projectview_hourly = dataset("hive_wmf_projectview_hourly").get_sensor_for(dag)

    allowlist_check = SparkSqlOperator(
        task_id="allowlist_check",
        sql=hql_path,
        query_parameters={
            "source_table": source_table,
            "allowlist_table": allowlist_table,
            "destination_table": unexpected_values_table,
            "year": year,
            "month": month,
            "day": day,
            "coalesce_partitions": 1,
        },
    )

    # Branch operator to determine whether to send alert or not.
    check_unexpected = BranchPythonOperator(
        task_id="check_unexpected",
        python_callable=check_for_unexpected_values,
        op_kwargs={
            "hadoop_name_node": hadoop_name_node,
            "unexpected_values_path": unexpected_hdfs_path,
        },
    )

    # Operator for sending an alert email (does not get run unless the branch above returns it)
    send_unexpected_email = EmailOperator(
        task_id="send_unexpected_email",
        to=unexpected_email,
        subject="Unexpected found in pageview hourly {{ dag.dag_id }} {{ ds }}",
        html_content=(
            # we could pass the contents of the file from the previous task
            # with file.read(), but it feels imprudent to send it in email
            "Found unexpected values.<br/>"
            "Please have a look and take necessary action !<br/>"
            "For the list of values, run:<br/>"
            "  select *<br/>"
            "  from wmf.pageview_unexpected_values<br/>"
            "  where year = {{data_interval_start.year}}<br/>"
            "    and month = {{data_interval_start.month}}<br/>"
            "    and day =  {{data_interval_start.day}};<br/>"
            "<br/>"
            "Thanks :)<br/>"
            "-- Airflow"
        ),
    )

    sense_projectview_hourly >> allowlist_check >> check_unexpected >> send_unexpected_email
