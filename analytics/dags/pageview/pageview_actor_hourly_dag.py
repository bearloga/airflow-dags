"""
### Compute the pageview_actor dataset every new hour

"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    artifact,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "pageview_actor_hourly"
var_props = VariableProperties(f"{dag_id}_config")

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    # Same as webrequest_actor_label_hourly
    start_date=var_props.get_datetime("start_date", datetime(2023, 1, 2)),
    schedule="@hourly",
    tags=[
        "hourly",
        "from_hive",
        "to_hive",
        "uses_hql",
        "requires_wmf_webrequest",
        "requires_wmf_webrequest_actor_label_hourly",
    ],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
) as dag:
    webrequest_sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)
    webrequest_actor_label_sensor = dataset("hive_wmf_webrequest_actor_label_hourly").get_sensor_for(dag)

    etl = SparkSqlOperator(
        task_id="compute_pageview_actor_hourly",
        sql=var_props.get("hql_path", f"{hql_directory}/pageview/actor/pageview_actor.hql"),
        query_parameters={
            "refinery_hive_jar_path": var_props.get(
                "refinery_hive_jar_path", artifact("refinery-hive-0.2.30-shaded.jar")
            ),
            "source_table": var_props.get("source_table", "wmf.webrequest"),
            "actor_label_table": var_props.get("actor_label_table", "wmf.webrequest_actor_label_hourly"),
            "destination_table": var_props.get("destination_table", "wmf.pageview_actor"),
            "coalesce_partitions": var_props.get("coalesce_partitions", 32),  # Number of output files
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
        },
        driver_memory=var_props.get("driver_memory", "4G"),
        executor_memory=var_props.get("executor_memory", "8G"),
        executor_cores=var_props.get("executor_cores", 2),
        conf={
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.executor.memoryOverhead": var_props.get("spark_executor_memoryOverhead", "4G"),
        },
    )

    # TODO: Remove this step when the various dependent tables are migrated to Airflow
    add_success_file = URLTouchOperator(
        task_id="write_pageview_actor_success_file",
        url=f"{hadoop_name_node}/wmf/data/wmf/pageview/actor"
        "/year={{data_interval_start.year}}"
        "/month={{data_interval_start.month}}"
        "/day={{data_interval_start.day}}"
        "/hour={{data_interval_start.hour}}"
        "/_SUCCESS",
    )

    [webrequest_sensor, webrequest_actor_label_sensor] >> etl >> add_success_file
