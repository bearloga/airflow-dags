"""
### Transfering data to the AQS hourly dataset
* Extracting data from the webrequest dataset (on an hourly partition)
* Filtering on the AQS requests
    - webrequest_source = 'text'
    - uri_path like '/api/rest_v1/metrics/%'
* Selecting statistically interesting dimensions.
* Loading them to a partition in the AQS hourly dataset.

The resulting dataset is:
* partitioned by year, month, day, hour
* located in /wmf/data/wmf/aqs/hourly

#### Note

* This job is waiting for the existence of a webrequest hive partition.
* The Hive script runs in Spark.
* We are using dynamicAllocation, but best results with this configuration:
    - 128 executors, with 2 cores each
    - spark.dynamicAllocation.initialExecutors = 128
    - spark.dynamicAllocation.maxExecutors = 256
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.datahub import DatahubLineageEmitterOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "aqs_hourly"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2022, 6, 14, 8),
    source_table="wmf.webrequest",
    webrequest_source="text",
    hql_path=f"{hql_directory}/aqs/hourly.hql",
    hql_iceberg_path=f"{hql_directory}/aqs/aqs_hourly_iceberg.hql",
    destination_table="wmf.aqs_hourly",
    destination_table_iceberg="wmf_traffic.aqs_hourly",
    sla=timedelta(hours=6),
    email=alerts_email,
)


with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@hourly",
    tags=["hourly", "from_hive", "to_hive", "uses_hql", "requires_wmf_webrequest"],
    sla=props.sla,
    email=props.email,
) as dag:
    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    run_aqs_hourly_hql = SparkSqlOperator(
        task_id="process_aqs_hourly",
        sql=props.hql_path,
        query_parameters={
            "source_table": props.source_table,
            "webrequest_source": props.webrequest_source,
            "destination_table": props.destination_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
            "coalesce_partitions": 8,
        },
    )

    run_aqs_hourly_hql_iceberg = SparkSqlOperator(
        task_id="process_aqs_hourly_iceberg",
        sql=props.hql_iceberg_path,
        query_parameters={
            "source_table": props.source_table,
            "webrequest_source": props.webrequest_source,
            "destination_table": props.destination_table_iceberg,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
            "coalesce_partitions": 1,
        },
    )

    emit_lineage = DatahubLineageEmitterOperator(
        downstream=dataset("hive_wmf_aqs_hourly"), upstreams=[dataset("hive_wmf_webrequest_all_sources")]
    )

    sensor >> [run_aqs_hourly_hql, run_aqs_hourly_hql_iceberg] >> emit_lineage
