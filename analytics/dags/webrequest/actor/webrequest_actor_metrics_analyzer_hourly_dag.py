from datetime import datetime

from analytics.config.dag_config import artifact, create_easy_dag, dataset
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.util import sanitize_string

dag_id = "webrequest_actor_metrics_analyzer_hourly"

props = DagProperties(
    start_date=datetime(2023, 12, 1),
    application=artifact("refinery-job-0.2.45-shaded.jar"),  # TODO: not yet released.
    table="wmf.webrequest_actor_metrics_hourly",
    metrics_table="wmf_data_ops.data_quality_metrics",
    # we should refactor this to a global config
)


with create_easy_dag(dag_id=dag_id, start_date=props.start_date, schedule="@hourly") as dag:
    year = "{{ data_interval_start.year }}"
    month = "{{ data_interval_start.month }}"
    day = "{{ data_interval_start.day }}"
    hour = "{{ data_interval_start.hour }}"

    wait_for_actor_hourly = dataset("hive_wmf_webrequest_actor_metrics_hourly").get_sensor_for(dag)

    compute_metrics = SparkSubmitOperator(
        task_id="actor_metrics_metrics",
        application=props.application,
        java_class="org.wikimedia.analytics.refinery.job.dataquality.ActorSignatureMetrics",
        application_args={
            "--table": props.table,
            "--metrics_table": props.metrics_table,
            "--partition_map": f"year:{year},month:{month},day:{day},hour:{hour}",
            "--run_id": sanitize_string("{{ run_id }}"),
        },
    )

    wait_for_actor_hourly >> compute_metrics
