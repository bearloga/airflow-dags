from os import environ, path

from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.dataset import DatasetRegistry
from wmf_airflow_common.easy_dag import EasyDAGFactory
from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.util import is_wmf_airflow_instance

# MARK: Paths

GITLAB_WMDE_ANALYTICS_RAW_DIR = "https://gitlab.wikimedia.org/repos/wmde/analytics/-/raw/main"
GITLAB_WMDE_HQL_JOBS_DIR = f"{GITLAB_WMDE_ANALYTICS_RAW_DIR}/hql/airflow_jobs"
GITLAB_WMDE_SPARK_JOBS_DIR = f"{GITLAB_WMDE_ANALYTICS_RAW_DIR}/spark/airflow_jobs"

HADOOP_NAME_NODE = "hdfs://analytics-hadoop"
HDFS_PUBLISHED_DIR = f"{HADOOP_NAME_NODE}/wmf/data/published"
HDFS_PUBLISHED_DATASETS_DIR = f"{HDFS_PUBLISHED_DIR}/datasets"
HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR = f"{HDFS_PUBLISHED_DATASETS_DIR}/wmde/analytics"
HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR = f"{HADOOP_NAME_NODE}/tmp/wmde/analytics/airflow"

# MARK: Identifiers

DAILY = "daily"
WEEKLY = "weekly"
MONTHLY = "monthly"

SENSOR = "sensor"
COMPUTE = "compute"
GEN_CSV = "gen_csv"
ARCHIVE_CSV = "archive_csv"

# MARK: Schedules

WEEKLY_STARTING_ON_MONDAY = "0 0 * * 1"

# MARK: Defaults

WMDE_ANALYTICS_ALERTS_EMAIL = "analytics@wikimedia.de"

instance_default_args = {
    "owner": "analytics-wmde",
    "email": WMDE_ANALYTICS_ALERTS_EMAIL,
    "hadoop_name_node": HADOOP_NAME_NODE,
    "metastore_conn_id": "analytics-hive",
}

spark_sql_operator_default_args = {
    "driver_cores": 2,
    "driver_memory": "4G",
    "executor_cores": 4,
    "executor_memory": "8G",
    "conf": {
        "spark.dynamicAllocation.maxExecutors": 16,
        "spark.yarn.executor.memoryOverhead": 2048,
    },
}

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args["queue"] = "production"

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in dags.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance("wmde")
artifact = artifact_registry.artifact_url

# Dataset registry for easier Sensor configuration.
# Provides a syntactic sugar to use when creating Sensors in DAGs.
dataset_file_paths = [path.join(path.dirname(__file__), "datasets.yaml")]
dataset_registry = DatasetRegistry(dataset_file_paths)
dataset = dataset_registry.get_dataset

_extra_default_args = {
    **instance_default_args,
    # Arguments for operators that have artifact dependencies
    "hdfs_tools_shaded_jar_path": artifact("hdfs-tools-0.0.6-shaded.jar"),
    "spark_sql_driver_jar_path": artifact("wmf-sparksqlclidriver-1.0.0.jar"),
}

if not is_wmf_airflow_instance():
    user = environ.get("USER")
    if user:
        _extra_default_args["owner"] = user

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get(_extra_default_args)

# Helper for creating DAGs with a nicer interface.
# Passes all the defaults and shortcuts to the DAG factory.
create_easy_dag = EasyDAGFactory(
    factory_default_args={
        **default_args,
        # Add here more default args dictionaries you want the easy_dag to have.
    },
    factory_default_args_shortcuts=["email", "sla"],
    factory_user_defined_filters=filters,
).create_easy_dag
