"""
Derives daily aggregates of Wikidata edits based on the device type of the user.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_device_type_edits
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPUTE,
    DAILY,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

TASK_ID = "wd_device_type_edits"
DAG_ID = f"{TASK_ID}_{DAILY}"
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{TASK_ID}"

WD_DEVICE_TYPE_EDITS = TASK_ID
WD_DEVICE_TYPE_EDITS_DAILY = DAG_ID
GEN_CSV_WD_DEVICE_TYPE_EDITS_DAILY = f"{GEN_CSV}_{WD_DEVICE_TYPE_EDITS_DAILY}"

# MARK: Properties

props = DagProperties(
    # HDFS source tables:
    hive_mediawiki_revision_create_table="event.mediawiki_revision_create",
    hive_mediawiki_revision_tags_change_table="event.mediawiki_revision_tags_change",
    # HDFS destination table:
    hive_wmde_wd_device_type_edits_daily=f"wmde.{WD_DEVICE_TYPE_EDITS_DAILY}",
    # Task Hive query:
    hql_wd_device_type_edits_daily=f"{GITLAB_HQL_TASK_DIR}/{WD_DEVICE_TYPE_EDITS_DAILY}.hql",
    # TMP export directory:
    tmp_dir_wd_device_type_edits_daily=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_DEVICE_TYPE_EDITS_DAILY}",
    # TMP export query:
    hql_gen_csv_wd_device_type_edits_daily=f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV_WD_DEVICE_TYPE_EDITS_DAILY}.hql",
    # Archive export directory:
    pub_data_dir_wd_device_type_edits_daily=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_DEVICE_TYPE_EDITS_DAILY}"
    ),
    # Archive export file:
    archive_csv_wd_device_type_edits_daily=f"{WD_DEVICE_TYPE_EDITS_DAILY}.csv",
    # Metadata:
    start_date=datetime(2024, 7, 1),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "from_hive",
        "requires_event_mediawiki_revision_create",
        "requires_event_mediawiki_revision_tags_change",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=DAG_ID,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensors

    sensor_revision_create = dataset("hive_event_mediawiki_revision_create").get_sensor_for(dag)
    sensor_revision_tags_change = dataset("hive_event_mediawiki_revision_tags_change").get_sensor_for(dag)

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_DEVICE_TYPE_EDITS_DAILY}",
        sql=props.hql_wd_device_type_edits_daily,
        query_parameters={
            "source_mw_revision_create": props.hive_mediawiki_revision_create_table,
            "source_mw_revision_tags_change": props.hive_mediawiki_revision_tags_change_table,
            "destination_table": props.hive_wmde_wd_device_type_edits_daily,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    # gen_csv = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_DEVICE_TYPE_EDITS_DAILY}",
    #     sql=props.hql_gen_csv_wd_device_type_edits_daily,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_device_type_edits_daily,
    #         "destination_directory": props.tmp_dir_wd_device_type_edits_daily,
    #     },
    # )

    # MARK: Archive Datasets

    # archive_csv = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_DEVICE_TYPE_EDITS_DAILY}",
    #     source_directory=props.tmp_dir_wd_device_type_edits_daily,
    #     archive_file=(
    #         props.pub_data_dir_wd_device_type_edits_daily + "/" + props.archive_csv_wd_device_type_edits_daily
    #     ),
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # MARK: Execute DAG

    [sensor_revision_create, sensor_revision_tags_change] >> compute  # >> gen_csv >> archive_csv
