"""
Derives monthly sets of the user agents that are accessing the Wikidata REST API as well as their request totals.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_rest_api_user_agents
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPUTE,
    GITLAB_WMDE_HQL_JOBS_DIR,
    MONTHLY,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

TASK_ID = "wd_rest_api_user_agents"
DAG_ID = f"{TASK_ID}_{MONTHLY}"
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{TASK_ID}"

WD_REST_API_USER_AGENTS = TASK_ID
WD_REST_API_USER_AGENTS_MONTHLY = DAG_ID

# MARK: Properties

props = DagProperties(
    # HDFS source table:
    hive_webrequest_table="wmf.webrequest",
    # HDFS destination table:
    hive_wmde_wd_rest_api_metrics_monthly=f"wmde.{WD_REST_API_USER_AGENTS_MONTHLY}",
    # Task Hive query:
    hql_wd_rest_api_metrics_monthly=f"{GITLAB_HQL_TASK_DIR}/{WD_REST_API_USER_AGENTS_MONTHLY}.hql",
    # Metadata:
    start_date=datetime(2024, 7, 1),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "from_hive",
        "monthly",
        "requires_wmf_webrequest",
        "to_hive",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=DAG_ID,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_REST_API_USER_AGENTS_MONTHLY}",
        sql=props.hql_wd_rest_api_metrics_monthly,
        query_parameters={
            "source_table": props.hive_webrequest_table,
            "destination_table": props.hive_wmde_wd_rest_api_metrics_monthly,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Execute DAG

    sensor >> compute
