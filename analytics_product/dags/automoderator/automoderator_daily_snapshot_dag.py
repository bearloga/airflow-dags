"""
Fetches & processes a daily snapshot of Automoderator's activity

Steps:
    - gets the list of dbs to fetch the snaphots for, from Automoderator's config.
    - fetches the snapshots from MariaDB replicas.
    - each snapshot is appended to wmf_product.automoderator_monitoring_snapshot_daily.
    - a latest snapshot is published as TSV to https://analytics.wikimedia.org/published/datasets/.
    - snapshots older than 30 days will be purged from the destination table.

Variables for testing:
{
  "start_date": "2024-09-27T00:00:00",
  "sla": "P1D",
  "destination_table": "kcvelaga.automoderator_monitoring_snapshot_daily",
  "alerts_email": "kcvelaga@wikimedia.org",
  "conda_env": "hdfs://analytics-hadoop/user/kcvelaga/artifacts/automoderator-metrics-jobs-0.2.0-v0.2.0.conda.tgz",
  "customdata_jar_path": "hdfs:///user/milimetric/customdata.jar",
  "mariadb_pw_file_path": "hdfs://path/to/test/mariadb-pw.txt",
  "mysql_mariadb_driver": "hdfs:///wmf/cache/artifacts/airflow/analytics_product/mysql-connector-j-8.2.0.jar",
  "base_output_path": "hdfs://analytics-hadoop/tmp/kcvelaga/automoderator/daily_snapshots_archive",
  "tmp_directory": "hdfs://analytics-hadoop/tmp/kcvelaga/automoderator/daily_snaphots",
  "driver_memory": "4G",
  "driver_cores": "4",
  "executor_memory": "8G",
  "executor_cores": "8",
  "max_executors": "16",
  "automod_config_url": "https://analytics.wikimedia.org/published/datasets/automoderator/automoderator_config.tsv",
  "purge_thresold": 30,
  "time_partition_col": "snapshot_date"
}
"""

from airflow.operators.dummy import DummyOperator
from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    artifact,
    hadoop_name_node,
    hdfs_temp_directory
)

from typing import List
import requests
import csv

from wmf_airflow_common import util
from airflow.decorators import task, task_group
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from airflow.operators.python import get_current_context

WIKIDBS_XCOM_KEY = "wiki_dbs"

def _push_wikidbs_to_xcom(dbs):
    get_current_context()["task_instance"].xcom_push(key=WIKIDBS_XCOM_KEY, value=dbs)
    
props = DagProperties(
    
    start_date=datetime(2024, 10, 2),
    sla=timedelta(days=1),
    catchup=False,

    destination_table="wmf_product.automoderator_monitoring_snapshot_daily",
    alerts_email=product_analytics_alerts_email,

    conda_env=artifact("automoderator-metrics-jobs-0.3.0-v0.3.0.conda.tgz"),
    customdata_jar_path=artifact("refinery-spark-0.2.51.jar"),
    mysql_mariadb_driver=artifact("mysql-connector-j-8.2.0.jar"),

    mariadb_pw_file_path="/user/analytics-product/mysql-analytics-research-client-pw.txt",
    
    # paths for HDFSArchiveOperator
    # note: the intermediate directories will be created by the operator as needed
    base_output_path=f"{hadoop_name_node}/wmf/data/published/datasets/automoderator/daily_snaphots",
    tmp_directory=f"{hadoop_name_node}{hdfs_temp_directory}/automoderator/daily_snaphots",

    driver_memory="8G",
    driver_cores="4",
    executor_memory="8G",
    executor_cores="8",
    max_executors="32",
    # temporary fix to reduce concurrency; more context in T376113
    # can be removed once Airflow to dse-k8s cluster (T362788)
    max_active_tasks=2,

    automod_config_url="https://analytics.wikimedia.org/published/datasets/automoderator/automoderator_config.tsv",
    purge_thresold=30,
    time_partition_col='snapshot_date',
)

with create_easy_dag(
    dag_id="automoderator_monitoring_snapshot_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=["daily", "to_iceberg", "automoderator", "from_mariadb", "web_publication"],
    sla=props.sla,
    email=props.alerts_email,
    max_active_runs=1,
    max_active_tasks=props.max_active_tasks, 
    catchup=props.catchup,
    default_args = {
        "map_index_template": f"{{{{ ti.xcom_pull(key='{WIKIDBS_XCOM_KEY}')[ti.map_index] }}}}",
    }
) as dag: 
    
    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    spark_conf = {}
    util.dict_add_or_append_string_value(spark_conf, "spark.jars", props.mysql_mariadb_driver, ",")
    util.dict_add_or_append_string_value(spark_conf, "spark.jars", props.customdata_jar_path, ",")
    
    @task(do_xcom_push=True)
    def fetch_configured_dbs(url=props.automod_config_url) -> List[str]:
        response = requests.get(
            url=url,
            headers={
                "User-Agent": f"WMF-Airflow-Analytics-{dag.dag_id}/1.0.0 "
                "(https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags)"
            },
            timeout=10,
        )
        response.raise_for_status()
        config = response.text
        dbs = []
        csv_reader = csv.DictReader(config.splitlines(), delimiter='\t')
        for row in csv_reader:
            dbs.append(row['wiki_db'])
        dbs = sorted(list(set(dbs)))
        _push_wikidbs_to_xcom(dbs)
        return dbs

    @task_group("fetch_snapshots")
    def fetch_snapshots(wiki_db: str):

        db_xcom_var = f"{{{{ ti.xcom_pull(key='{WIKIDBS_XCOM_KEY}')[ti.map_index] }}}}"
        tmp_file_path=f"{props.tmp_directory}/{db_xcom_var}"
        
        fetch_daily_snapshot = SparkSubmitOperator.for_virtualenv(
            task_id="fetch_automoderator_daily_snapshot",
            virtualenv_archive=props.conda_env,
            entry_point="bin/fetch_automoderator_daily_snapshot.py",
            driver_memory=props.driver_memory,
            driver_cores=props.driver_cores,
            executor_memory=props.executor_memory,
            executor_cores=props.executor_cores,
            max_active_tis_per_dag=props.max_active_tasks,
            sla=None,
            conf=spark_conf,
            launcher="skein",
            application_args=[
                "--wiki_db",
                wiki_db,
                "--destination_table",
                props.destination_table,
                "--tmp_dir_path",
                tmp_file_path,
                "--mariadb_pw_file",
                props.mariadb_pw_file_path
            ],
            use_virtualenv_spark=True,
        )

        fetch_daily_snapshot

    @task_group("web_publication")
    def web_publication(wiki_db: str):

        db_xcom_var = f"{{{{ ti.xcom_pull(key='{WIKIDBS_XCOM_KEY}')[ti.map_index] }}}}"
        output_file_path = f"{props.base_output_path}/{db_xcom_var}_automoderator_daily_snapshot.tsv"
        tmp_file_path=f"{props.tmp_directory}/{db_xcom_var}"

        publish_tsv = HDFSArchiveOperator(
            task_id="publish_automoderator_daily_snapshot_tsv",
            source_directory=tmp_file_path,
            archive_file=output_file_path,
            expected_filename_ending=".csv",
            sla=None,
            max_active_tis_per_dag=props.max_active_tasks,
            check_done=True
        )

        publish_tsv

    purge_snapshots = SparkSubmitOperator.for_virtualenv(
        task_id="purge_automoderator_daily_snapshots",
        virtualenv_archive=props.conda_env,
        entry_point="bin/purge_automoderator_daily_snapshots.py",
        launcher="skein",
        application_args=[
            "--destination_table",
            props.destination_table,
            "--time_partition_col",
            props.time_partition_col,
            "--threshold",
            props.purge_thresold
        ],
        use_virtualenv_spark=True,
        sla=None,
    )

    dbs = fetch_configured_dbs()
    start >> fetch_snapshots.expand(wiki_db=dbs) >> web_publication.expand(wiki_db=dbs) >> purge_snapshots >> end
