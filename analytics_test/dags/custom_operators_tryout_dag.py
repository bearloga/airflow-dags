"""
The goal of this dag is to test custom operators on the test cluster.

Please, let it on pause when not testing.

We check that the action is triggered without problem, not the result of the action itself.
* test_time_filters should wait indefinitely for its partition
* test_url_touch_operator should succeed
* test_url_sensor should succeed
* test_hdfs_archive_operator should fail with ans HDFSArchiver error (target is not a directory)

What could go wrong:
* The call the sub-library has changed and needs update (fsspec, pyarrow, ...).
* A change in the internals of Airflow needs some adaptation in our custom operators.
"""

from datetime import datetime

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from analytics_test.config.dag_config import default_args, hadoop_name_node
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

dag_id = "custom_operators_tryout"
var_props = VariableProperties(f"{dag_id}_config")

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 1, 17)),
    schedule="@daily",
    tags=["test"],
    user_defined_filters=filters,
    default_args=var_props.get_merged("default_args", {**default_args}),
) as dag:
    # This operator should wait indefinitely for its partition.
    sensor1 = NamedHivePartitionSensor(
        task_id="test_time_filters",
        partition_names=["wmf.webrequest/snapshot={{data_interval_start | start_of_current_month | to_ds}}"],
    )

    touched_file = f"{hadoop_name_node}/tmp/test_URLTouchOperator"
    operator1 = URLTouchOperator(task_id="test_url_touch_operator", url=touched_file)

    sensor2 = URLSensor(task_id="test_url_sensor", url=(f"{hadoop_name_node}/wmf/data/raw/unknow_table"))

    # This operator should eventually crash with a message:
    #   ERROR HDFSArchiver Dir <touched_file> is not a directory.
    # It's expected.
    operator2 = HDFSArchiveOperator(
        task_id="test_hdfs_archive_operator",
        source_directory=touched_file,
        expected_filename_ending=".csv",
        archive_file=f"{hadoop_name_node}/tmp/test_HDFSArchiveOperator",
        check_done=True,
    )

    operator1 >> sensor2 >> operator2
