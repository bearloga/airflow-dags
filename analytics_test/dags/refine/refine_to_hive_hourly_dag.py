from datetime import datetime

from analytics.dags.refine.refine_to_hive_hourly_dag_factory import (
    abs_path_to_file_beside_this_file,
    generate_dag,
)
from analytics_test.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    hadoop_name_node,
)
from wmf_airflow_common.config.dag_properties import DagProperties

dag_id = "refine_to_hive_hourly_test"

props = DagProperties(
    start_date=datetime(2024, 8, 8, 17),
    email=alerts_email,
    event_stream_config_url=f"file://{abs_path_to_file_beside_this_file('event_stream_config.json', __file__)}",
    refinery_job_jar=artifact("refinery-job-0.2.50-shaded.jar"),
    catchup=False,
    max_active_tasks=10,
    max_active_runs=1,
    # Change the next parameters after the staging phase
    output_database="event_alt",
    output_tables_base_location=f"{hadoop_name_node}/user/analytics/event_alt",
    diff_database="event",  # Used for diff check. Will diff the partition created with the one from this process.
    pyspark_extension_pkg="hdfs:///user/analytics/pyspark_extension-2.12.0.3.1.tar.gz",
)

generate_dag(  # This generates an Airflow DAG https://github.com/apache/airflow/blob/2.5.1/airflow/utils/file.py#L358
    dag_id, props, __name__, create_easy_dag, hadoop_name_node
)
