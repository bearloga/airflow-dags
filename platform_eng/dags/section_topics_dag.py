#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate weekly section topics: Wikidata items from Wikipedia blue links.
See https://gitlab.wikimedia.org/repos/structured-data/section-topics
"""

from datetime import datetime, timedelta
from os import path

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, hadoop_name_node
from platform_eng.config.utils import latest_html_dumps_snapshot_before

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from mergedeep import merge

props = DagProperties(
    # DAG
    alerts_email="sd-alerts@lists.wikimedia.org",
    dag_id="section_topics",
    start_date=datetime(2022, 9, 7),
    schedule="0 0 * * 1",  # The DAG starts on Mondays at midnight
    timeout=timedelta(days=6),  # Times out on Sundays, before the next run
    catchup=False,
    tags=["structured-data-team"],
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",
    detect_html_tables_path_snapshot="",
    # Sensors
    sensors_poke_interval=timedelta(hours=1).total_seconds(),  # Check every hour
    # Spark job
    executor_memory="8G",
    conda_env=artifact("section-topics-0.16.0-v0.16.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section_topics",
)
args = merge(
    {},
    default_args,
    {
        "email": [default_email, props.alerts_email],
        # Large job as per
        # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Large_jobs,
        # https://github.com/wikimedia/wmfdata-python/blob/v2.0.0/wmfdata/spark.py#L50
        "driver_memory": "4G",
        "executor_cores": 4,
        "executor_memory": props.executor_memory,
        "conf": {
            "spark.sql.shuffle.partitions": 2048,
            "spark.reducer.maxReqsInFlight": 1,
            "spark.shuffle.io.retryWait": "60s",
            "spark.executor.memoryOverhead": "3g",
            "spark.dynamicAllocation.enabled": "true",
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.shuffle.service.enabled": "true",
            # Uncomment in case of `FetchFailed` due to `NetworkTimeout`, see
            # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Scaling_the_driver,
            # https://towardsdatascience.com/fetch-failed-exception-in-apache-spark-decrypting-the-most-common-causes-b8dff21075c
            # "spark.stage.maxConsecutiveAttempts": 10,
            # "spark.shuffle.io.maxRetries": 10,
        },
        # Avoid `skein.exceptions.DriverError: Received message larger than max (21089612 vs. 4194304)`
        "skein_app_log_collection_enabled": False,
    }
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_macros={
        'latest_html_dumps_snapshot_before': latest_html_dumps_snapshot_before,
    },
) as dag:
    # Pre-condition: wait for the latest input snapshots
    # NOTE We pass snapshot values via Airflow's built-in templates,
    #      see https://airflow.apache.org/docs/apache-airflow/2.6.3/templates-ref.html
    if not props.weekly_snapshot:
        weekly = "{{ data_interval_start.format('YYYY-MM-DD') }}"
    else:
        weekly = props.weekly_snapshot

    # Set the most recent monthly snapshot given the weekly one
    # NOTE A snapshot date is the *beginning* of the snapshot interval. For instance:
    #      - 2022-05-16 covers until 2022-05-22 (at 23:59:59).
    #        May is not over, so the May monthly snapshot is not available yet.
    #        Hence return end of *April*, i.e., 2022-04
    #      - 2022-05-30 covers until 2022-06-05.
    #        May is over, so the May monthly snapshot is available.
    #        Hence return end of *May*, i.e., 2022-05
    if not props.monthly_snapshot:
        monthly = "{{ data_interval_start.end_of('week').subtract(months=1).start_of('month').format('YYYY-MM') }}"
    else:
        monthly = props.monthly_snapshot

    if not props.detect_html_tables_path_snapshot:
        detect_html_tables_path_snapshot = "{{ latest_html_dumps_snapshot_before(data_interval_end) }}"
    else:
        detect_html_tables_path_snapshot = props.detect_html_tables_path_snapshot

    # wait for the Wikidata weekly table
    wait_for_wikidata = NamedHivePartitionSensor(
        task_id="wait_for_wikidata",
        partition_names=[f"wmf.wikidata_item_page_link/snapshot={weekly}"],
        poke_interval=props.sensors_poke_interval,
    )

    # wait for the wikitext monthly table
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    wait_for_wikitext = URLSensor(
        task_id="wait_for_wikitext",
        # URL for `wmf.mediawiki_wikitext_current`
        url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={monthly}/_PARTITIONED",
        poke_interval=props.sensors_poke_interval,
    )

    # wait for the weekly section titles denylist
    denylist_path = path.join(props.work_dir, 'section_titles_denylist', weekly)
    wait_for_section_titles_denylist = URLSensor(
        task_id="wait_for_section_titles_denylist",
        url=f"{hadoop_name_node}{denylist_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # wait for monthly check_bad_parsing.py output
    check_bad_parsing_path = path.join(props.work_dir, 'bad_parsing', monthly)
    wait_for_check_bad_parsing = URLSensor(
        task_id="wait_for_check_bad_parsing",
        url=f"{hadoop_name_node}{check_bad_parsing_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # wait for twice-monthly sections-with-html-tables data
    detect_html_tables_path = path.join(props.work_dir, 'target_wikis_tables', detect_html_tables_path_snapshot)
    wait_for_detect_html_tables = URLSensor(
        task_id="wait_for_detect_html_tables",
        url=f"{hadoop_name_node}{detect_html_tables_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # generate fresh fetch_qids_from_wikidata.py outputs
    qids_filters_sparql_dir = 'venv/lib/python3.10/site-packages/section_topics/scripts/sparql'
    qids_filters_output_dir = path.join(props.work_dir, 'qids_filters')
    qids_filters = {
        f"{qids_filters_sparql_dir}/qids_for_all_points_in_time.sparql": path.join(qids_filters_output_dir, 'qids_for_all_points_in_time', weekly),
        f"{qids_filters_sparql_dir}/qids_for_media_outlets.sparql": path.join(qids_filters_output_dir, 'qids_for_media_outlets', weekly),
    }
    fetch_qids_filters = [
        SparkSubmitOperator.for_virtualenv(
            task_id=f"fetch_{path.basename(sparql).split('.')[0]}",
            virtualenv_archive=props.conda_env,
            entry_point="lib/python3.10/site-packages/section_topics/scripts/fetch_qids_from_wikidata.py",
            application_args=[
                sparql,
                "--output", output,
            ],
            launcher="skein",
        ) for sparql, output in qids_filters.items()
    ]

    # remove all but the last 4 outputs for each fetch_qids_from_wikidata.py script
    old_files_cmd = ' && '.join([f"hdfs dfs -ls {path.dirname(output)} | head -n -4 | tr -s ' ' | cut -d' ' -f8" for sparql, output in qids_filters.items()])
    cleanup_fetch_qids_filters = SimpleSkeinOperator(
        task_id="cleanup_fetch_qids",
        script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $({old_files_cmd})",
    )

    # not currently used:
    # # generate fresh collect_media_prefixes.py output
    # media_prefixes_dir = path.join(props.work_dir, 'media_prefixes')
    # media_prefixes_path = path.join(media_prefixes_dir, weekly)
    # collect_media_prefixes = SparkSubmitOperator.for_virtualenv(
    #     task_id="collect_media_prefixes",
    #     virtualenv_archive=props.conda_env,
    #     entry_point="lib/python3.10/site-packages/section_topics/scripts/collect_media_prefixes.py",
    #     application_args=[
    #         "--output", media_prefixes_path,
    #     ],
    #     launcher="skein",
    # )
    # # remove all but the last 4 outputs
    # cleanup_collect_media_prefixes = SimpleSkeinOperator(
    #     task_id="cleanup_collect_media_prefixes",
    #     script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $(hdfs dfs -ls {media_prefixes_dir} | head -n -4 | tr -s ' ' | cut -d' ' -f8)",
    # )

    # execute the pipeline with default optional arguments
    pipeline = SparkSubmitOperator.for_virtualenv(
        task_id="pipeline",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/pipeline.py",
        application_args=[
            weekly,
            "--output", path.join(props.work_dir, weekly),
            "--page-filter", check_bad_parsing_path,
            "--section-title-filter", denylist_path,
            "--table-filter", detect_html_tables_path,
            *[arg for sparql, output in qids_filters.items() for arg in ["--qid-filter", output]],
            # not currently used:
            # "--handle-media", path.join(props.work_dir, 'media_links', weekly),
            # "--media-prefixes", media_prefixes_path,
        ],
        launcher="skein",
    )

    [
        wait_for_wikidata,
        wait_for_wikitext,
        wait_for_section_titles_denylist,
        wait_for_check_bad_parsing,
        wait_for_detect_html_tables,
        fetch_qids_filters >> cleanup_fetch_qids_filters,
        # not currently used:
        # collect_media_prefixes >> cleanup_collect_media_prefixes,
    ] >> pipeline
