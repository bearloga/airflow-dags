#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate bi-weekly Wikipedia article sections with HTML tables:
this is required input for the section topics data pipeline, see
https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/a728c2b18f3cc982c85228d9cb06e678adcf73ec/section_topics/pipeline.py#L774
"""

from datetime import datetime, timedelta

from os import path

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, wikis
from platform_eng.config.utils import latest_html_dumps_snapshot_before

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

from airflow import DAG
from airflow.operators.bash import BashOperator

from mergedeep import merge

props = DagProperties(
    # DAG
    alerts_email="sd-alerts@lists.wikimedia.org",
    dag_id="detect_html_tables",
    start_date=datetime(2024, 9, 18),
    # per https://dumps.wikimedia.org/other/enterprise_html/, the dumps are
    # supposed to become available "around" the 2nd and 21st; let's add
    # another day to that to make sure
    schedule="0 0 3,22 * *",  # The DAG runs on the 3rd & 22th
    timeout=timedelta(days=28),  # Times out after 4 weeks
    catchup=False,
    tags=["structured-data-team"],
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    snapshot="",
    # Sensors
    sensors_poke_interval=timedelta(hours=1).total_seconds(),  # Check every hour
    conda_env=artifact("section-topics-0.16.0-v0.16.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section_topics",
    nfs_dump_path="/mnt/nfs/dumps-clouddumps1001.wikimedia.org/other/enterprise_html/runs/{snapshot}/{wiki}-NS0-{snapshot}-ENTERPRISE-HTML.json.tar.gz",
)
args = merge(
    {},
    default_args,
    {
        "email": [default_email, props.alerts_email],
    }
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_macros={
        'latest_html_dumps_snapshot_before': latest_html_dumps_snapshot_before,
    }
) as dag:
    if not props.snapshot:
        # note: using data_interval_end, because that's *right before* this DAG runs:
        # the run scheduled on 22nd covers the interval from 3rd to 22nd, and we
        # want to process the snapshot of the 20th, that just became available on the
        # 21st; data_interval_start would refer to the 3rd, and we'd be processing
        # that snapshot a little late...
        snapshot = "{{ latest_html_dumps_snapshot_before(data_interval_end) }}"
    else:
        snapshot = props.snapshot

    nfs_dump_path = props.nfs_dump_path
    hdfs_dump_dir = path.join(props.work_dir, 'dumps', '{snapshot}')

    wait_for_dumps = [
        URLSensor(
            task_id="wait_for_dump_{wiki}".format(wiki=wiki),
            url=nfs_dump_path.format(snapshot=snapshot, wiki=wiki),
            poke_interval=props.sensors_poke_interval,
        ) for wiki in wikis
    ]

    create_hdfs_path = SimpleSkeinOperator(
        task_id="create_hdfs_path",
        script=f"hdfs dfs -mkdir -p {hdfs_dump_dir.format(snapshot=snapshot)}",
    )

    move_dumps_to_hdfs = BashOperator(
        # note: BashOperator instead of SimpleSkeinOperator because the latter
        # executes in an env without access to the required NFS share
        task_id="move_dumps_to_hdfs",
        bash_command=f"hdfs dfs -copyFromLocal -f {' '.join([nfs_dump_path.format(snapshot=snapshot, wiki=wiki) for wiki in wikis])} {hdfs_dump_dir.format(snapshot=snapshot)}",
    )

    detect_html_tables_dir = path.join(props.work_dir, 'target_wikis_tables')
    detect_html_tables_path = path.join(detect_html_tables_dir, snapshot)
    detect_html_tables = SparkSubmitOperator.for_virtualenv(
        task_id="detect_html_tables",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/scripts/detect_html_tables.py",
        application_args=[
            snapshot,
            "--dumps-path", path.join(hdfs_dump_dir, '{wiki}-NS0-{snapshot}-ENTERPRISE-HTML.json.tar.gz'),
            "--wikis", *wikis,
            "--output", detect_html_tables_path,
        ],
        launcher="skein",
    )

    # remove temp dump copies & all but the last 4 outputs
    cleanup_detect_html_tables = [
        SimpleSkeinOperator(
            task_id="cleanup_dumps_from_hdfs",
            script=f"hdfs dfs -rm -r {hdfs_dump_dir.format(snapshot=snapshot)}",
        ), SimpleSkeinOperator(
            task_id="cleanup_detect_html_tables",
            script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $(hdfs dfs -ls {detect_html_tables_dir} | head -n -4 | tr -s ' ' | cut -d' ' -f8)",
        ),
    ]

    wait_for_dumps >> create_hdfs_path >> move_dumps_to_hdfs >> detect_html_tables >> cleanup_detect_html_tables
