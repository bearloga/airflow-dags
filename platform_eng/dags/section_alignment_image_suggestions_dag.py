#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate weekly section-level image suggestions based on cross-wiki section alignment.
See https://gitlab.wikimedia.org/repos/structured-data/section-image-recs

Important default variables within the DAG:

    - ``article_images_path`` (article images output HDFS parquet) =
        /user/analytics-platform-eng/structured-data/section-alignment-suggestions/article_images/{weekly}
    - ``suggestions_output`` (image suggestions output HDFS parquet) =
        /user/analytics-platform-eng/structured-data/section-alignment-suggestions/suggestions/{weekly}
    - ``wp_codes`` (Wikipedia language codes to process) = []
        Stands for all Wikipedias
    - ``seal_path`` (machine-learned section alignments model input HDFS parquet) =
        /user/analytics-platform-eng/structured-data/seal/alignments/{weekly}
    - ``max_target_images`` (max images that a suggested section contains) = 0
        Stands for unillustrated sections only

You can override defaults in the Airflow Web UI - Admin > Variables:
Key is the ``props`` name and Value is the overridden value.

Remember to wait until your DAG gets parsed again.
Check it in the UI - Details > DagModel debug information > last_parsed_time.
"""

from datetime import datetime, timedelta
from os import path

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, hadoop_name_node
from platform_eng.config.utils import latest_html_dumps_snapshot_before

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.partitions_builder import (
    PrePartitions,
    add_post_partitions,
)
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from mergedeep import merge

props = DagProperties(
    # DAG
    alerts_email="sd-alerts@lists.wikimedia.org",
    dag_id="section_alignment_image_suggestions",
    start_date=datetime(2023, 1, 1),
    schedule="0 0 * * 1",  # The DAG starts on Mondays at midnight
    timeout=timedelta(days=6),  # Times out on Sundays, before the next run
    catchup=False,
    tags=["structured-data-team"],
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",
    # Sensors
    sensors_poke_interval=timedelta(hours=1).total_seconds(),  # Check every hour
    # Spark jobs
    conda_env=artifact("imagerec-0.11.0-v0.11.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section-alignment-suggestions",
    article_images_dir="article_images",
    article_images_executor_memory="8G",
    suggestions_dir="suggestions",
    suggestions_executor_memory="8G",
    wp_codes=[],  # Means: process all Wikipedias. Pass a list of language codes to override
    seal_path="/user/analytics-platform-eng/structured-data/seal/alignments/{weekly}",  # Must be absolute
    detect_html_tables_path="/user/analytics-platform-eng/structured-data/section_topics/target_wikis_tables/{html_biweekly}",
    max_target_images=0,  # Emit suggestions for sections with no images
)
args = merge(
    {},
    default_args,
    {
        "email": [default_email, props.alerts_email],
    }
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
    user_defined_macros={
        'latest_html_dumps_snapshot_before': latest_html_dumps_snapshot_before,
    },
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/2.6.3/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        weekly = "{{ data_interval_start | to_ds }}"
    else:
        weekly = props.weekly_snapshot
    # Set the most recent monthly snapshot given the weekly one.
    # Here's the template chain with examples:
    # data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month
    # 2023-05-01          | 2023-05-07          | 2023-04-01              | "2023-04"
    # 2023-05-29          | 2023-06-04          | 2023-05-01              | "2023-05"
    if not props.monthly_snapshot:
        monthly = "{{ data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month }}"
    else:
        monthly = props.monthly_snapshot
    html_biweekly = "{{ latest_html_dumps_snapshot_before(data_interval_end) }}"

    # Initial setup for sensors: whole Wikidata snapshot, wikitext task ID, poke every hour
    wikidata_partitions = [f"wmf.wikidata_item_page_link/snapshot={weekly}"]
    wikitext_sensor_task_id = "wait_for_wmf.mediawiki_wikitext_current"

    # The default behavior is to process all Wikipedias.
    # Done via the `--wp-code-file` CLI arg, which defaults to `imagerec/data/wikipedias.json` in the imagerec package.
    wp_codes = props.wp_codes

    # `format` won't affect any overridden value
    seal_path = props.seal_path.format(weekly=weekly)
    detect_html_tables_path = props.detect_html_tables_path.format(html_biweekly=html_biweekly)

    # Wikitext sensor.
    # Default behavior first: wait for the whole wikitext snapshot with a `URLSensor`.
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    if not wp_codes:
        wait_for_wikitext = URLSensor(
            task_id=wikitext_sensor_task_id,
            # `wmf.mediawiki_wikitext_current` URL
            url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={monthly}/_PARTITIONED",
            poke_interval=props.sensors_poke_interval,
        )
    # Wait for the given Wikipedia partitions, which are sub-partitions of a snapshot.
    # Use a `NamedHivePartitionSensor`.
    else:
        wiki_db_partitions = PrePartitions([[f"wiki_db={code}wiki" for code in wp_codes]])
        wikidata_partitions = add_post_partitions(wikidata_partitions, wiki_db_partitions)
        wikitext_partitions = add_post_partitions(
            [f"wmf.mediawiki_wikitext_current/snapshot={monthly}"], wiki_db_partitions
        )
        wait_for_wikitext = NamedHivePartitionSensor(
            task_id=wikitext_sensor_task_id,
            partition_names=wikitext_partitions,
            poke_interval=props.sensors_poke_interval,
        )

    # Wikidata sensor
    wait_for_wikidata = NamedHivePartitionSensor(
        task_id="wait_for_wmf.wikidata_item_page_link",
        partition_names=wikidata_partitions,
        poke_interval=props.sensors_poke_interval,
    )

    # SEAL sensor
    # NOTE The _SUCCESS file tells us that the data has finished being written
    wait_for_seal = URLSensor(
        task_id="wait_for_seal",
        url=f"{hadoop_name_node}/{seal_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # Sensor for sections with tables
    wait_for_detect_html_tables = URLSensor(
        task_id="wait_for_detect_html_tables",
        url=f"{hadoop_name_node}{detect_html_tables_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # Gather Wikipedia article images
    # The final path can't be in `props`: the `weekly` template is rendered inside the `DAG` context manager
    article_images_path = f"{props.work_dir}/{props.article_images_dir}/{weekly}"
    article_images_args = [
        "--wikitext-snapshot", monthly,
        "--item-page-link-snapshot", weekly,
        "--output", article_images_path,
    ]
    if wp_codes:
        article_images_args.append("--wp-codes")
        article_images_args += wp_codes

    gather_article_images = SparkSubmitOperator.for_virtualenv(
        task_id="gather_article_images",
        driver_memory="16G",
        executor_cores=4,
        executor_memory=props.article_images_executor_memory,
        conf={
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.sql.shuffle.partitions": 1024,
        },
        virtualenv_archive=props.conda_env,
        entry_point="bin/imagerec_article_images.py",
        application_args=article_images_args,
        launcher="skein",
    )

    # Generate section alignment suggestions
    # The final path can't be in `props`: the `weekly` template is rendered inside the `DAG` context manager
    suggestions_output = f"{props.work_dir}/{props.suggestions_dir}/{weekly}"
    suggestions_args = [
        "--output", suggestions_output,
        "--section-images", article_images_path,
        "--section-alignments", seal_path,
        "--max-target-images", props.max_target_images,
        "--table-filter", detect_html_tables_path,
    ]
    if wp_codes:
        suggestions_args.append("--wp-codes")
        suggestions_args += wp_codes

    generate_suggestions = SparkSubmitOperator.for_virtualenv(
        task_id="generate_suggestions",
        driver_memory="16G",
        executor_cores=4,
        executor_memory=props.suggestions_executor_memory,
        conf={
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.sql.shuffle.partitions": 1024,
        },
        virtualenv_archive=props.conda_env,
        # TODO rename https://gitlab.wikimedia.org/repos/structured-data/section-image-recs/-/blob/da47561aacda0248a7ec1baffbdaaa370aa43ae2/pyproject.toml#L15
        entry_point="bin/imagerec_recommendation.py",
        application_args=suggestions_args,
        launcher="skein",
    )

    # Build the DAG
    (
        [
            wait_for_wikidata,
            wait_for_wikitext,
            wait_for_seal,
            wait_for_detect_html_tables,
        ]
        >> gather_article_images
        >> generate_suggestions
    )
