"""A DAG to execute the full knowledge gaps pipeline.
"""

from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.decorators import task_group
from airflow.operators.empty import EmptyOperator

from research.config import dag_config
from research.dags import snapshot_sensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "knowledge_gaps_development_db_dag"
var_props = VariableProperties(f"{dag_id}_config")

# snapshots that the sensors will wait for
mediawiki_snapshot_template = "{{data_interval_start | to_ds_month}}"
wikidata_snapshot_template = "{{data_interval_start | start_of_current_week | to_ds}}"
mediawiki_snapshot = var_props.get("mediawiki_snapshot", mediawiki_snapshot_template)
wikidata_snapshot = var_props.get("wikidata_snapshot", wikidata_snapshot_template)


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 9, 1)),
    schedule="@monthly",
    dagrun_timeout=timedelta(days=30),
    catchup=False,
    user_defined_filters=filters,
    tags=["spark", "hive", "research", "knowledge gaps"],
    default_args={
        **var_props.get_merged("default_args", dag_config.default_args),
    },
) as dag:
    hdfs_temp_directory = var_props.get(
        "hdfs_temp_directory", dag_config.hdfs_temp_directory
    )
    run_dir = "{{ ts_nodash }}"
    hdfs_dir = os.path.join(hdfs_temp_directory, "knowledge_gaps", run_dir)
    hdfs_dir = var_props.get("hdfs_dir", hdfs_dir)

    output_database = var_props.get("output_database", "knowledge_gaps_dev")
    table_prefix = var_props.get("table_prefix", "dev_")

    publish_dir = var_props.get(
        "publish_dir", "/wmf/data/published/datasets/knowledge_gaps/content_gaps/"
    )

    time_bucket_freq = var_props.get("time_bucket_freq", "monthly")

    wikis = var_props.get("wikis", "'elwiki'")

    database_name = var_props.get("database_name", "development_data")

    common_params = {
        "launcher": "skein",
        "driver_memory": "4G",
        # As of 07/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        "skein_app_log_collection_enabled": True,
        # to keep the skein containers running for development
        # 'command_postamble': '; sleep 600',
    }

    spark_conf = {
        "spark.shuffle.service.enabled": "true",
        "spark.executor.memoryOverhead": "2048",
        # 'spark.sql.sources.partitionOverwriteMode': 'dynamic',
        "spark.dynamicAllocation.enabled": "true",
        # 'spark.dynamicAllocation.initialExecutors': 5,
    }

    def create_development_database():
        """
        Generate data for development and testing of knowledge gaps pipeline
        """
        if var_props.get("skip_database_generation", False):
            return EmptyOperator(task_id="generate_development_database")

        return SparkSqlOperator(
            task_id="generate_development_database",
            sql="https://gitlab.wikimedia.org/repos/research/knowledge-gaps/-/raw/9bf00dda519bd5ed1076a497ef520533870bc769/knowledge_gaps/config/development.sql",
            query_parameters={
                "mediawiki_snapshot": f"{mediawiki_snapshot}",
                "wikidata_snapshot": f"{wikidata_snapshot}",
                "wikis": wikis,
                "database_name": database_name,
                "table_prefix": "dev_",
            },
            driver_cores=2,
            driver_memory="4G",
            executor_cores=4,
            executor_memory="8G",
            conf={
                "spark.yarn.executor.memoryOverhead": 2048,
                "hive.exec.dynamic.partition.mode": "nonstrict",
            },
        )

    @task_group(group_id="knowledge_gaps_sensors")
    def sensors():
        _ = [
            snapshot_sensor.wait_for_article_features(mediawiki_snapshot),
            snapshot_sensor.wait_for_mediawiki_page_history_snapshot(
                mediawiki_snapshot
            ),
            snapshot_sensor.wait_for_mediawiki_page_snapshot(mediawiki_snapshot),
            snapshot_sensor.wait_for_mediawiki_revision_snapshot(mediawiki_snapshot),
            snapshot_sensor.wait_for_wikidata_item_page_link_snapshot(
                wikidata_snapshot
            ),
            snapshot_sensor.wait_for_wikidata_entity_snapshot(wikidata_snapshot),
        ]

    _ = sensors() >> create_development_database()
