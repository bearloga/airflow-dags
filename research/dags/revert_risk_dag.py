"""
Collection of dags for the revert risk model
"""

import getpass
import os
import tempfile
from dataclasses import field
from datetime import datetime
from pathlib import Path

import fsspec
from airflow.decorators import dag, task, task_group
from airflow.operators.bash import BashOperator
from airflow.operators.email import EmailOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.task_group import TaskGroup
from pydantic.dataclasses import dataclass
from workflow_utils import util

from research.config import dag_config
from research.config.args import (
    ModelKind,
    Period,
    RevertriskBasefeaturesRun,
    RevertriskEvaluateRunmodelcomparison,
    RevertriskEvaluateRunmodelevaluation,
    RevertriskFeaturesRun,
    RevertriskPredictRun,
    RevertriskTrainRun,
)
from research.config.wikis import WIKIS
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    # General pipeline arguments
    # The model kind, e.g. "language-agnostic", "multilingual"
    model: ModelKind = ModelKind.language_agnostic
    snapshot: str = "{{ data_interval_start | to_ds_month }}"
    start_time: str = "{{ data_interval_start | to_ds }}"
    end_time: str = "{{ data_interval_end | to_ds }}"
    wikis: list[str] = field(default_factory=lambda: WIKIS)
    hdfs_dir: Path = (
        # The directory used to store intermediate data
        Path(dag_config.hdfs_temp_directory)
        / "revert_risk"
    )
    # Train / test dataset arguments
    training_samples_per_wiki: int = 300_000
    disputed_reverts: bool = False
    anonymous_edits: bool = True
    test_split: float = 0.3
    # Model evaluation
    reference_model_uri: str = (
        "https://analytics.wikimedia.org/published/wmf-ml-models/revertrisk/language-agnostic/20231117132654/model.pkl"
    )
    # Emails that receive model promotion decisions
    model_promote_emails: list[str] | None = None
    # Spark configuration
    # number of executors for training (dynamic allocation not supported)
    train_executors: int = 15
    # max exectutors for non-training pipelines
    max_executors: int = 98
    shuffle_partitions: int = 512
    driver_memory: str = "8G"
    executor_cores: int = 4
    executor_memory: str = "12G"
    executor_memory_overhead: str = "4G"


props = DagProperties.from_variable("revert_risk")
default_args = dag_config.default_args | {
    "do_xcom_push": True,
}

username = getpass.getuser()


@dag(
    schedule=None,
    start_date=datetime(2024, 4, 1),
    catchup=False,
    tags=["research", "ml", "feature"],
    user_defined_filters=filters,
    default_args=default_args,
)
def base_features_pipeline():
    @task
    def make_args(snapshot: str, start_time: str, end_time: str) -> str:
        return RevertriskBasefeaturesRun(
            snapshot=snapshot,
            wikis=props.wikis,
            output=props.hdfs_dir / "base_features_dataset",
            period=Period(
                start=datetime.fromisoformat(start_time),
                end=datetime.fromisoformat(end_time),
            ),
        ).json()

    spark_conf = {
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }

    SparkSubmitOperator.for_virtualenv(
        task_id="generate_features_dataset",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=[
            "revertrisk.basefeatures.run",
            make_args(
                snapshot=props.snapshot,
                start_time=props.start_time,
                end_time=props.end_time,
            ),
        ],
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
    )


base_features_pipeline()


def features_dataset(
    base_features_path: Path, training_dataset_path: Path, test_dataset_path: Path
) -> SparkSubmitOperator:
    spark_conf = {
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }
    args = RevertriskFeaturesRun(
        base_features=base_features_path,
        output=training_dataset_path,
        model=props.model,
        partition_output=["wiki_db", 4],
        split_dataset=[str(test_dataset_path), props.test_split],
        max_rows_per_wiki=props.training_samples_per_wiki,
        disputed_reverts=props.disputed_reverts,
        anonymous_edits=props.anonymous_edits,
    )

    return SparkSubmitOperator.for_virtualenv(
        task_id="generate_features_dataset",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["revertrisk.features.run", args.json()],
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
    )


@task_group()
def train_model(training_dataset_path: Path, model_uri: str) -> None:
    spark_conf = {
        # "spark.locality.wait": 0,
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }
    args = RevertriskTrainRun(
        training_dataset=training_dataset_path,
        model_uri=model_uri,
    )
    train_model_job = SparkSubmitOperator.for_virtualenv(
        task_id="train_model",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["revertrisk.train.run", args.json()],
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        # configure fixed number of executors, as dynamic allocation is not supported
        # by the pyspark xgboost integration
        num_executors=props.train_executors,
        conf=spark_conf,
    )

    # @task.bash
    # def chmod_model() -> str:
    #     return f"hfds dfs -chmod 755 {model_uri}"
    chmod_model = BashOperator(task_id="chmod_model", bash_command=f"hdfs dfs -chmod 755 {model_uri}")

    _ = train_model_job >> chmod_model


def batch_predict(model_uri: str, features_dataset_path: Path, prediction_path: Path) -> SparkSubmitOperator:
    spark_conf = {
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }
    args = RevertriskPredictRun(
        features_dataset=features_dataset_path,
        output=prediction_path,
        model_uri=model_uri,
    )
    return SparkSubmitOperator.for_virtualenv(
        task_id="batch_predict",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["revertrisk.predict.run", args.json()],
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
        default_env_vars={
            "https_proxy": "http://webproxy.eqiad.wmnet:8080",
            "REQUESTS_CA_BUNDLE": "/etc/ssl/certs/ca-certificates.crt",
        },
    )


def model_metrics(predictions_dataset: Path, model_dir: Path) -> SparkSubmitOperator:
    spark_conf = {
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }
    args = RevertriskEvaluateRunmodelevaluation(predictions_dataset=predictions_dataset, output=model_dir)
    return SparkSubmitOperator.for_virtualenv(
        task_id="model_metrics",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["revertrisk.evaluate.runmodelevaluation", args.json()],
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
    )


def evaluate_model(task_id: str, model_uri: str, features_dataset_path: Path, model_dir: Path) -> TaskGroup:
    with TaskGroup(task_id) as eval:
        predictions_dataset = model_dir / "eval_predictions"
        predictions = batch_predict(model_uri, features_dataset_path, predictions_dataset)
        metrics = model_metrics(predictions_dataset, model_dir)
        _ = predictions >> metrics
    return eval


@task_group
def promote_model(candidate_model_dir: Path, reference_model_dir: Path, promote_dir: Path) -> None:
    spark_conf = {
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }
    args = RevertriskEvaluateRunmodelcomparison(
        candidate_model_dir=candidate_model_dir,
        reference_model_dir=reference_model_dir,
        output=promote_dir,
    )
    compare_models = SparkSubmitOperator.for_virtualenv(
        task_id="compare_models",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["revertrisk.evaluate.runmodelcomparison", args.json()],
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
    )

    if props.model_promote_emails:
        decision_file = str(promote_dir / "DECISION")
        description_file = str(promote_dir / "description.txt")
        auc_diff_file = str(promote_dir / "auc_diff.csv")

        util.fsspec_use_new_pyarrow_api(True)
        fs = fsspec.filesystem("hdfs")

        @task
        def embedded_file(file_path: str) -> str:
            file = fs.open(file_path)
            return file.read()

        @task
        def attach_file(file_path: str) -> str:
            file = fs.open(file_path)
            content = file.read()
            temp_file = Path(tempfile.gettempdir()) / file_path.split("/")[-1]
            open(temp_file, mode="wb").write(content)
            return str(temp_file)

        @task
        def delete_file(file_path: str):
            os.remove(file_path)

        with TaskGroup("read_decision") as read_decision:
            attachement = attach_file(auc_diff_file)
            decision = embedded_file(decision_file)
            description = embedded_file(description_file)

        emails = [
            EmailOperator(
                task_id=f"promote_model_email_{i}",
                to=email,
                subject=f"Model Promotion Decision: Revert risk model ({props.model})",
                html_content=(f"Decision: {decision}<br/>" f"Description: <br/>" f"{description}<br/>"),
                files=[attachement],
                # dag=dag,
            )
            for i, email in enumerate(props.model_promote_emails)
        ]

        _ = compare_models >> read_decision >> emails >> delete_file(attachement)

    else:
        _ = compare_models >> EmptyOperator(task_id="no_promote_model_emails")


@dag(
    schedule=None,
    start_date=datetime(2024, 4, 1),
    catchup=False,
    tags=["research", "revert_risk", "ml", "training"],
    user_defined_filters=filters,
    default_args=default_args,
)
def revert_risk_pipeline():
    base_features_path = props.hdfs_dir / "base_features_dataset"
    features_dataset_dir = props.hdfs_dir / "features_dataset"
    training_dataset_path = features_dataset_dir / "train"
    test_dataset_path = features_dataset_dir / "test"

    candidate_model_dir = props.hdfs_dir / "model"
    candidate_model_uri = f"""{dag_config.hadoop_name_node}{candidate_model_dir / "binary" / "model.pkl"}"""
    promote_decision_dir = props.hdfs_dir / "decision"

    reference_model_dir = props.hdfs_dir / "reference_model"

    datasets = features_dataset(base_features_path, training_dataset_path, test_dataset_path)
    model = train_model(training_dataset_path, candidate_model_uri)
    model_evals = [
        evaluate_model(
            task_id="evalute_candidate_model",
            model_uri=candidate_model_uri,
            features_dataset_path=test_dataset_path,
            model_dir=candidate_model_dir,
        ),
        evaluate_model(
            task_id="evalute_reference_model",
            model_uri=props.reference_model_uri,
            features_dataset_path=test_dataset_path,
            model_dir=reference_model_dir,
        ),
    ]

    promote = promote_model(candidate_model_dir, reference_model_dir, promote_decision_dir)

    _ = datasets >> model >> model_evals >> promote


revert_risk_pipeline()
