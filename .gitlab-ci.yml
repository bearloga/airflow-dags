include:
  - project: 'repos/releng/kokkuri'
    file:
      - includes/images.yaml

stages:
  - build
  - test
  - publish

workflow:
  rules:
    # Run for merge request events
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    # Run for protected tags
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED

variables:

  # Debian package name
  PACKAGE_NAME: airflow

  # Airflow debian package version
  PACKAGE_VERSION: 2.10.2-py3.10-20241002

  # Path to the Dockerfile generating the deb file
  PACKAGE_DOCKERFILE: debian/Dockerfile

  # Path to the Dockerfile within the Docker image
  PACKAGE_DOCKER_FILENAME: /srv/airflow/airflow-${PACKAGE_VERSION}_amd64.deb

  # Bullseye image version
  BULLSEYE_VERSION: 20240128




# Docker images creation jobs

.build-conda-env-base-image:
  extends: .kokkuri:build-and-publish-image
  stage: build
  variables:
    BUILD_CONFIG: docker/blubber.yml
    BUILD_TARGET_PLATFORMS: linux/amd64
    PUBLISH_IMAGE_EXTRA_TAGS: 'ci'

build-conda-env-test-image:
  extends: .build-conda-env-base-image
  variables:
    BUILD_VARIANT: build-test-image
    PUBLISH_IMAGE_TAG: "${PACKAGE_VERSION}-pipeline-${CI_PIPELINE_ID}-test"

build-image-conda-env-lint:
  extends: .build-conda-env-base-image
  variables:
    BUILD_VARIANT: build-lint-image
    PUBLISH_IMAGE_TAG: "${PACKAGE_VERSION}-pipeline-${CI_PIPELINE_ID}-lint"



# Unit test job

.activate-conda-script:
  - source /srv/app/miniconda/bin/activate
  - conda activate airflow

# Note: The test image used for pytest is missing the Pyspark jars which are not used in a unit test environment.
pytest:
  stage: test
  image: ${BUILD_CONDA_ENV_TEST_IMAGE_IMAGE_REF}
  script:
    - !reference [.activate-conda-script]
    - >
      PYTHONPATH=.:wmf_airflow_common/plugins
      COVERAGE_FILE=/tmp/.coverage
      pytest
      --runslow
      --cov=wmf_airflow_common
      --cov analytics_test
      --cov-report=term
      --cov-report=xml:coverage.xml
      -svv
      --junitxml=junit_pytest_report.xml
      -o cache_dir=/tmp/pytest_cache_dir &&
      ls -lh
  # Match coverage total from job log output.
  # See: https://docs.gitlab.com/ee/ci/yaml/index.html#coverage
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    when: always
    reports:
      # Publish coverage.xml as an artifact, so it can be used in the GitLab CI UI.
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      # Publish pytest report in junit xml format, so it can be used in the GitLab CI UI.
      junit: junit_pytest_report.xml



# Linting jobs

.linting-base:
  image: ${BUILD_IMAGE_CONDA_ENV_LINT_IMAGE_REF}
  stage: test

flake8:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - flake8 --version
    - flake8

mypy:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - mypy --version
    - mypy

black:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - black --check .

isort:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - isort --check .



# Linting for the debian/Dockerfile

lint-dockerfile:
  image: 'docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION}'
  stage: test
  only:
    changes:
      - debian/Dockerfile
  script:
    - apt-get -qq update && apt-get -qq install -y wget ca-certificates
    - wget --quiet -O /bin/hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64
    - echo "56de6d5e5ec427e17b74fa48d51271c7fc0d61244bf5c90e828aab8362d55010 /bin/hadolint" > /tmp/hadolintcheck && \
    - sha256sum --check --strict /tmp/hadolintcheck && \
    - chmod +x /bin/hadolint
    - hadolint --config debian/hadolint.yml --failure-threshold warning debian/Dockerfile



# Generate the deb file

.install_docker_cli:
  # Update the apt package index and install packages to allow apt to use a repository over HTTPS:
  - apt-get update
  - >
    apt-get --yes install
    ca-certificates
    curl
    gnupg
    lsb-release
  # Add Docker’s official GPG key:
  - mkdir -p /etc/apt/keyrings
  - curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
  #  set up the docker repository:
  - >
    echo
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
  - apt-get update
  # now, install & test the CLI
  - apt install --yes docker-ce-cli
  - docker info

publish-debian-package:
  stage: publish
  image: docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION}
  variables:
    BUILD_CONFIG: debian/Dockerfile
    BUILD_TARGET_PLATFORMS: linux/amd64
    BUILD_VARIANT: publish_deb
    GENERIC_PACKAGE_REGISTRY_URI: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"
  tags: [docker]
  rules:
    - when: manual
      allow_failure: true
  script:
    - echo "WARNING - We are currently using our custom runner (`docker` tag) to generate the deb file. More info here https://phabricator.wikimedia.org/T351792"
    - !reference [.install_docker_cli]
    # clean up
    - docker system prune --force
    # print available disk
    - df -H
    # print current images
    - docker images
    # build package (most of the build time should be here)
    - >
      docker build
      --platform linux/amd64
      --file ${PACKAGE_DOCKERFILE}
      --tag ${PACKAGE_NAME}
      --build-arg PACKAGE_VERSION=${PACKAGE_VERSION}
      --build-arg GENERIC_PACKAGE_REGISTRY_URI=${GENERIC_PACKAGE_REGISTRY_URI}
      --build-arg CI_JOB_TOKEN=$CI_JOB_TOKEN
      --target publish_deb
      .

