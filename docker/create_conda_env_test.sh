set -e
set +x
source miniconda/bin/activate
conda activate airflow
pip install --quiet .[test]
rm miniconda/envs/airflow/share/py4j/py4j0.10.9.jar \
miniconda/envs/airflow/lib/python3.10/site-packages/skein/java/skein.jar \
miniconda/envs/airflow/lib/python3.10/site-packages/pyspark/jars/*
rm -Rf miniconda/pkgs
