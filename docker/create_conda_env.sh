set -e
set +x
source miniconda/bin/activate
conda install --yes --name base conda-libmamba-solver
conda config --set solver libmamba
conda env create --quiet --file conda-environment.lock.yml --name airflow
