import pytest


@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['search', 'dags', 'webrequest_metrics.py']

def test_sensor(dagbag, render_task):
    dag = dagbag.get_dag('webrequest_metrics_daily')
    task = dag.get_task('wait_for_data')
    rendered_task = render_task(task)
    rendered_task.pre_execute()

    expect = {
        f'wmf.webrequest/webrequest_source=text/year=2024/month=3/day=3/hour={hour}'
        for hour in range(0, 24)
    }
    assert set(rendered_task.partition_names) == expect
