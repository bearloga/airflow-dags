import pytest
from airflow.models import DagBag


@pytest.fixture
def dag_path() -> list[str]:
    return ["research", "dags", "article_topics_dag.py"]


def test_article_topics_dag_loaded(dagbag: DagBag) -> None:
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="article_topics")
    assert dag is not None
    assert len(dag.tasks) == 12
