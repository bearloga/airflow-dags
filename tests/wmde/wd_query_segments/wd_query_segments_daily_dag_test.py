"""
Tests for wd_query_segments_daily_dag.
"""

import pytest


# This fixture defines the dag_path for the shared dagbag one.
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["wmde", "dags", "wd_query_segments", "wd_query_segments_daily_dag.py"]


def test_wd_query_segments_daily_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wd_query_segments_daily")
    assert dag is not None
    assert len(dag.tasks) == 2  # 4
