import boto3
import pytest
from freezegun import freeze_time
from moto import mock_aws

from test_k8s.dags.cleanup_s3_logs_dag import purge_old_logs_from_s3


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["test_k8s", "dags", "cleanup_s3_logs_dag.py"]


@pytest.fixture()
def airflow_variables(monkeypatch):
    monkeypatch.setenv("AIRFLOW_VAR_S3_LOG_RETENTION_DAYS", "30")
    monkeypatch.setenv("AIRFLOW_VAR_S3_LOG_BUCKET", "logs.airflow-test")


@pytest.fixture(scope="function")
def s3():
    """
    Return a mocked S3 client
    """
    with mock_aws():
        s3_cli = boto3.client("s3")
        yield s3_cli


@pytest.fixture
def bucket(s3):
    s3.create_bucket(Bucket="logs.airflow-test")


@pytest.fixture
def logfiles(s3):
    # recent logs
    s3.put_object(
        Body=b"",
        Bucket="logs.airflow-test",
        Key="dag_id=addition/run_id=scheduled__2024-09-21T08:00:00+00:00/task_id=add_one_and_two/attempt=1.log",
    )
    s3.put_object(
        Body=b"",
        Bucket="logs.airflow-test",
        Key="dag_id=addition/run_id=scheduled__2024-09-20T08:00:00+00:00/task_id=add_one_and_two/attempt=1.log",
    )
    # old log
    s3.put_object(
        Body=b"",
        Bucket="logs.airflow-test",
        Key="dag_id=addition/run_id=scheduled__2024-06-21T08:00:00+00:00/task_id=add_one_and_two/attempt=1.log",
    )


@freeze_time("2024-09-26")
def test_purge_old_logs_from_s3(airflow_variables, s3, bucket, logfiles, mocker):
    mocker.patch("test_k8s.dags.cleanup_s3_logs_dag.get_s3_client", return_value=s3)
    assert purge_old_logs_from_s3() == 1
    assert len(s3.list_objects(Bucket="logs.airflow-test")["Contents"]) == 2


def test_dag_structure(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="purge_old_logs_from_s3")
    assert dag is not None
    assert dag.task_count == 1
    assert callable(dag.task_dict["purge_old_logs_from_s3"].python_callable)
    assert "airflow_maintenance" in dag.tags
