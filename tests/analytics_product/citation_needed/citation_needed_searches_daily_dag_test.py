import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "citation_needed", "citation_needed_searches_daily_dag.py"]


def test_citation_needed_searches_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="citation_needed_searches_daily")
    assert dag is not None
    assert len(dag.tasks) == 2

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False
