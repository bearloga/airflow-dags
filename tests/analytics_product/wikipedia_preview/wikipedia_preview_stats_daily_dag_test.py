import pytest


@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics_product', 'dags', 'wikipedia_preview',
        'wikipedia_preview_stats_daily_dag.py']


def test_wikipedia_preview_stats_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id='wikipedia_preview_stats_daily')
    assert dag is not None
    assert len(dag.tasks) == 2
