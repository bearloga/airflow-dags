import pytest


@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "editor_month", "editor_month_dag.py"]


def test_editor_month_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="editor_month")
    assert dag is not None
    assert len(dag.tasks) == 2

    # Tests that the defaults from dag_config are here. metastore_conn_id is used by the
    # NamedHivePartitionSensor for mediawiki_history.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"

    assert dag.default_args["email"] == "movement-insights+alerts@wikimedia.org"
