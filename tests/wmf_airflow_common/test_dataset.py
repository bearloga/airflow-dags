from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from delayed_timetables import DelayedDailyTimetable
from pytest import raises

from wmf_airflow_common.dataset import (
    DruidDataset,
    HiveDataset,
    HiveSnapshotDataset,
    IcebergDataset,
)
from wmf_airflow_common.sensors.druid import DruidSegmentSensor
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor


def test_invalid_table_name_hive_or_iceberg_dataset(monkeypatch):
    with raises(ValueError):
        HiveDataset(
            table_name="invalid",
            partitioning="@hourly",
        )
    with raises(ValueError):
        IcebergDataset(
            table_name="invalid",
        )


def test_invalid_partitioning_hive_dataset(monkeypatch):
    with raises(ValueError):
        HiveDataset(
            table_name="database.table",
            partitioning="@invalid",
        )


def test_invalid_pre_partitions_hive_dataset(monkeypatch):
    with raises(ValueError):
        HiveDataset(table_name="database.table", partitioning="@hourly", pre_partitions=["invalid"])


def test_hive_dataset_get_same_partitioning_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@daily",
        pre_partitions=["name=value"],
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@daily",
        start_date=datetime(2023, 1, 1),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.partition_names == [
        "database.table/name=value/year={{data_interval_start.year}}/"
        + "month={{data_interval_start.month}}/day={{data_interval_start.day}}"
    ]
    assert sensor.poke_interval == 900


def test_hive_dataset_get_hourly_to_daily_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@hourly",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@daily",
        start_date=datetime(2023, 1, 1),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.partition_names == [
        "database.table/year={{ data_interval_start.year }}/month={{ data_interval_start.month }}/"
        + "day={{ data_interval_start.day }}/hour="
        + str(i)
        for i in range(24)
    ]
    assert sensor.poke_interval == 900


def test_hive_dataset_get_daily_to_weekly_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@daily",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="0 0 * * 1",  # Weekly starting Mondays
        start_date=datetime(2023, 1, 2),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, RangeHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.table_name == "database.table"
    assert sensor.from_timestamp == "{{data_interval_start}}"
    assert sensor.to_timestamp == "{{data_interval_start.add(days=7)}}"
    assert sensor.granularity == "@daily"
    assert sensor.poke_interval == 1800


def test_hive_dataset_get_daily_to_monthly_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@daily",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2023, 1, 2),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, RangeHivePartitionSensor)
    assert sensor.to_timestamp == "{{data_interval_start.add(months=1)}}"


def test_hive_dataset_get_custom_monthly_sensor(monkeypatch):
    dataset = HiveDataset(table_name="database.table", partitioning="@monthly", custom_partition_format="month=YYYY-MM")
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2023, 1, 2),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.partition_names == [
        "database.table/month=" "{{ data_interval_start.format('YYYY') }}" "-" "{{ data_interval_start.format('MM') }}"
    ]
    assert sensor.poke_interval == 3600


def test_hive_dataset_get_custom_daily_sensor(monkeypatch):
    dataset = HiveDataset(table_name="database.table", partitioning="@daily", custom_partition_format="date=YYYY-MM-DD")
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2023, 1, 2),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, RangeHivePartitionSensor)
    assert sensor.to_timestamp == "{{data_interval_start.add(months=1)}}"
    assert sensor.from_timestamp == "{{data_interval_start}}"

    # make sure to test what the actual partition names look like
    sensor.from_timestamp = "2023-01-02"
    sensor.to_timestamp = "2023-01-04"
    sensor.pre_execute()
    assert sensor.partition_names == ["database.table/date=2023-01-03", "database.table/date=2023-01-02"]


def test_hive_dataset_get_daily_sensor_from_custom_timetable(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@hourly",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule=DelayedDailyTimetable(timedelta(days=10)),
        start_date=datetime(2023, 3, 15),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.partition_names == [
        "database.table/year={{ data_interval_start.year }}/month={{ data_interval_start.month }}/"
        + "day={{ data_interval_start.day }}/hour="
        + str(i)
        for i in range(24)
    ]
    assert sensor.poke_interval == 900


def test_hive_dataset_get_weekly_snapshot_sensor(monkeypatch):
    dataset = HiveSnapshotDataset(
        table_name="database.table",
        partitioning="@weekly",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@weekly",
        start_date=datetime(2023, 1, 2),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.partition_names == ["database.table/snapshot={{ data_interval_start.format('YYYY-MM-DD') }}"]
    assert sensor.poke_interval == 1800


def test_hive_dataset_get_monthly_snapshot_sensor(monkeypatch):
    dataset = HiveSnapshotDataset(
        table_name="database.table",
        partitioning="@monthly",
        pre_partitions=[["dc=1", "dc=2"]],
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2023, 1, 1),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.partition_names == [
        "database.table/dc=1/snapshot={{ data_interval_start.format('YYYY-MM') }}",
        "database.table/dc=2/snapshot={{ data_interval_start.format('YYYY-MM') }}",
    ]
    assert sensor.poke_interval == 3600


def test_druid_dataset_get_monthly_sensor(monkeypatch):
    dataset = DruidDataset(
        datasource_name="database_table",
        segment_granularity="@monthly",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2023, 1, 1),
        default_args={
            "druid_api_host": "some host",
            "druid_api_port": "1234",
        },
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, DruidSegmentSensor)
    assert sensor.task_id == "wait_for_database_table_segments"
    assert sensor.from_timestamp == "{{data_interval_start.isoformat()}}"
    assert sensor.to_timestamp == "{{data_interval_end.isoformat()}}"
    assert sensor.granularity == "@monthly"
    assert sensor.poke_interval == 3600


def test_druid_dataset_get_hourly_to_daily_sensor(monkeypatch):
    dataset = DruidDataset(
        datasource_name="database_table",
        segment_granularity="@hourly",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@daily",
        start_date=datetime(2023, 1, 1),
        default_args={
            "druid_api_host": "some host",
            "druid_api_port": "1234",
        },
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, DruidSegmentSensor)
    assert sensor.task_id == "wait_for_database_table_segments"
    assert sensor.from_timestamp == "{{data_interval_start.isoformat()}}"
    assert sensor.to_timestamp == "{{data_interval_end.isoformat()}}"
    assert sensor.granularity == "@hourly"
    assert sensor.poke_interval == 900


def test_iceberg_dataset_default_maintenance(monkeypatch):
    dataset = IcebergDataset(table_name="database.table", maintenance=None)

    # we want routine maintenance to be enabled by default
    # but data deletion disabled by default, to avoid rogue deletes
    # and data rewrite disabled by default, as it may only be needed when using merge-on-read
    assert dataset.maintenance["enabled"] is True
    assert dataset.maintenance["data_delete"]["enabled"] is False
    assert dataset.maintenance["rewrite_data_files"]["enabled"] is False


def test_iceberg_dataset_override_default_maintenance(monkeypatch):
    dataset = IcebergDataset(
        table_name="database.table",
        maintenance={"data_delete": {"enabled": True, "where": "a < 100 AND b == 123"}},
    )

    assert dataset.maintenance["enabled"] is True
    assert dataset.maintenance["data_delete"]["enabled"] is True
    assert dataset.maintenance["data_delete"]["where"] == "a < 100 AND b == 123"


def test_iceberg_dataset_maintenance_schedule_support(monkeypatch):
    # hourly not supported for now
    with raises(ValueError):
        IcebergDataset(table_name="database.table", maintenance={"schedule": "@hourly"})
    # custom not supported for now
    with raises(ValueError):
        IcebergDataset(table_name="database.table", maintenance={"schedule": "0 0 1 * *"})
    # daily, weekly, monthly all supported
    assert IcebergDataset(table_name="database.table", maintenance={"schedule": "@daily"}) is not None
    assert IcebergDataset(table_name="database.table", maintenance={"schedule": "@weekly"}) is not None
    assert IcebergDataset(table_name="database.table", maintenance={"schedule": "@monthly"}) is not None


def test_iceberg_dataset_remove_orphan_files_maintenance_task(monkeypatch):
    dataset = IcebergDataset(table_name="database.table")
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2024, 1, 1),
        default_args={
            "spark_sql_driver_jar_path": "spark_sql_driver.jar",
        },
    )

    tasks = dataset.get_maintenance_tasks(dag=dag, spark_conf={})
    assert len(tasks) == 3
    assert str(tasks[0]._sql) == (
        "CALL spark_catalog.system.remove_orphan_files( "
        "table => 'database.table', "
        "older_than => TIMESTAMP '{{ data_interval_end | subtract_days(2) | to_dt() }}', "
        "max_concurrent_deletes => 10 "
        ")"
    )


def test_iceberg_dataset_expire_snapshots_maintenance_task(monkeypatch):
    dataset = IcebergDataset(table_name="database.table")
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2024, 1, 1),
        default_args={
            "spark_sql_driver_jar_path": "spark_sql_driver.jar",
        },
    )

    tasks = dataset.get_maintenance_tasks(dag=dag, spark_conf={})
    assert len(tasks) == 3
    assert str(tasks[1]._sql) == (
        "CALL spark_catalog.system.expire_snapshots( "
        "table => 'database.table', "
        "older_than => TIMESTAMP '{{ data_interval_end | subtract_days(90) | to_dt() }}', "
        "max_concurrent_deletes => 10, "
        "stream_results => true "
        ")"
    )


def test_iceberg_dataset_rewrite_manifests_maintenance_task(monkeypatch):
    dataset = IcebergDataset(table_name="database.table")
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2024, 1, 1),
        default_args={
            "spark_sql_driver_jar_path": "spark_sql_driver.jar",
        },
    )

    tasks = dataset.get_maintenance_tasks(dag=dag, spark_conf={})
    assert len(tasks) == 3
    assert str(tasks[2]._sql) == "CALL spark_catalog.system.rewrite_manifests( table => 'database.table' )"


def test_iceberg_dataset_data_delete_maintenance_task(monkeypatch):
    dataset = IcebergDataset(
        table_name="database.table",
        maintenance={"data_delete": {"enabled": True, "where": "a < 100 AND b == 123"}},
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2024, 1, 1),
        default_args={
            "spark_sql_driver_jar_path": "spark_sql_driver.jar",
        },
    )

    tasks = dataset.get_maintenance_tasks(dag=dag, spark_conf={})
    assert len(tasks) == 4
    assert str(tasks[3]._sql) == "DELETE FROM database.table WHERE a < 100 AND b == 123"


def test_iceberg_dataset_rewrite_data_files_maintenance_task(monkeypatch):
    dataset = IcebergDataset(
        table_name="database.table",
        maintenance={
            "rewrite_data_files": {
                "enabled": True,
                "where": "column_dt < NOW() - INTERVAL 30 DAYS",
                "options": {"key1": "val1", "key2": "val2"},
                "spark_kwargs": {"executor_cores": "123"},
            },
        },
    )
    dag = DAG(
        dag_id="test_dag",
        schedule="@monthly",
        start_date=datetime(2024, 1, 1),
        default_args={
            "spark_sql_driver_jar_path": "spark_sql_driver.jar",
        },
    )

    tasks = dataset.get_maintenance_tasks(dag=dag, spark_conf={})

    assert len(tasks) == 4
    assert str(tasks[3]._sql) == (
        "CALL spark_catalog.system.rewrite_data_files( "
        "table => 'database.table', "
        "where => 'column_dt < NOW() - INTERVAL 30 DAYS', "
        "options => map('key1', 'val1', 'key2', 'val2') "
        ")"
    )
    assert tasks[3]._executor_cores == "123"
