import pendulum

from wmf_airflow_common.partitions_builder import (
    _add_pre_partition_options,
    _build_partition_timestamps,
    _build_partitions_options,
    _get_partition_time_part,
    add_post_partitions,
    build_partition_names,
    daily_partitions,
    druid_intervals,
    hours_of_month,
    partition_names_by_granularity,
)


def test_partition_name_by_granularity():
    # without pre_partitions
    partitions = partition_names_by_granularity("wmf.aqshourly", "@hourly")
    assert partitions == [
        "wmf.aqshourly/year={{data_interval_start.year}}"
        "/month={{data_interval_start.month}}"
        "/day={{data_interval_start.day}}/"
        "hour={{data_interval_start.hour}}"
    ]
    # with pre_partitions
    partitions = partition_names_by_granularity(
        "wmf.aqs_hourly", "@hourly", pre_partitions=["webrequest_source=test_text"]
    )
    assert partitions == [
        "wmf.aqs_hourly/"
        "webrequest_source=test_text/"
        "year={{data_interval_start.year}}/"
        "month={{data_interval_start.month}}/"
        "day={{data_interval_start.day}}/"
        "hour={{data_interval_start.hour}}"
    ]
    # with custom_partition_format
    # with timestamp
    timestamp = pendulum.parse("2022-02-10T16:43:34.825955+01:00")
    partitions = partition_names_by_granularity(
        "wmf.aqs_hourly", "@monthly", timestamp=timestamp, custom_partition_format="month=YYYY-MM"
    )
    assert partitions == ["wmf.aqs_hourly/" "month=2022-02"]
    # without timestamp
    partitions = partition_names_by_granularity("wmf.aqs_hourly", "@monthly", custom_partition_format="month=YYYY-MM")
    assert partitions == [
        "wmf.aqs_hourly/" "month={{ data_interval_start.format('YYYY') }}" "-" "{{ data_interval_start.format('MM') }}"
    ]

    # with timestamp
    timestamp = pendulum.parse("2022-02-10T16:43:34.825955+01:00")
    partitions = partition_names_by_granularity(
        "wmf.aqs_hourly",
        "@hourly",
        timestamp=timestamp,
        pre_partitions=["webrequest_source=test_text"],
    )
    assert partitions == ["wmf.aqs_hourly/" "webrequest_source=test_text/" "year=2022/" "month=2/" "day=10/" "hour=16"]


def test_get_partition_part():
    assert _get_partition_time_part("month", None) == "month={{data_interval_start.month}}"
    timestamp = pendulum.parse("2022-02-10T16:43:34.825955+01:00")
    assert _get_partition_time_part("month", timestamp) == "month=2"


def test_build_pre_partitions_options():
    assert _build_partitions_options(["type=1", ["dc=a", "dc=b"]]) == ([["type=1", "dc=a"], ["type=1", "dc=b"]])
    assert _build_partitions_options([["dc=a", "dc=b"], "type=1"]) == ([["dc=a", "type=1"], ["dc=b", "type=1"]])
    assert _build_partitions_options([[1, 2, 3], 4, [5, 6]]) == (
        [[1, 4, 5], [1, 4, 6], [2, 4, 5], [2, 4, 6], [3, 4, 5], [3, 4, 6]]
    )


def test_add_pre_partition_options():
    assert _add_pre_partition_options(["wmf.entity", "year=2022"], ["dc=a", "t=1"]) == [
        ["wmf.entity", "dc=a", "t=1", "year=2022"]
    ]
    assert _add_pre_partition_options(["wmf.entity", "year=2022"], [["dc=a", "dc=b"]]) == [
        ["wmf.entity", "dc=a", "year=2022"],
        ["wmf.entity", "dc=b", "year=2022"],
    ]


def test_build_partition_names_hourly():
    assert build_partition_names(
        "event.mediawiki_page_move",
        "2022-01-29T22:00:00+00:00",
        "2022-01-30T03:00:00+00:00",
        "@hourly",
    ) == [
        "event.mediawiki_page_move/year=2022/month=1/day=30/hour=2",
        "event.mediawiki_page_move/year=2022/month=1/day=30/hour=1",
        "event.mediawiki_page_move/year=2022/month=1/day=30/hour=0",
        "event.mediawiki_page_move/year=2022/month=1/day=29/hour=23",
        "event.mediawiki_page_move/year=2022/month=1/day=29/hour=22",
    ]


def test_build_partition_names_daily():
    assert build_partition_names(
        "event.mediawiki_page_move",
        "2022-01-29T00:00:00+00:00",
        "2022-01-30T23:59:59.999999+00:00",
        "@daily",
        pre_partitions=[["datacenter=eqiad", "datacenter=codfw"]],
    ) == [
        "event.mediawiki_page_move/datacenter=codfw/year=2022/month=1/day=29",
        "event.mediawiki_page_move/datacenter=eqiad/year=2022/month=1/day=29",
    ]


def test_build_partition_names_monthly():
    assert build_partition_names(
        "event.mediawiki_page_move",
        "2022-01-01T00:00:00+00:00",
        "2022-02-01T00:00:00+00:00",
        "@monthly",
        pre_partitions=[["datacenter=eqiad", "datacenter=codfw"]],
    ) == [
        "event.mediawiki_page_move/datacenter=codfw/year=2022/month=1",
        "event.mediawiki_page_move/datacenter=eqiad/year=2022/month=1",
    ]


def test_build_partition_timestamps():
    from_ts = "2022-01-29T00:00:00+00:00"
    to_ts = "2022-01-30T23:59:59.999999+00:00"
    assert _build_partition_timestamps(from_ts, to_ts, "@daily") == [pendulum.parse(from_ts)]

    from_ts = "2022-01-29T00:00:00+00:00"
    assert _build_partition_timestamps(from_ts, from_ts, "@daily") == []

    from_ts = "2022-03-08T00:00:00+00:00"
    to_ts = "2022-03-09T01:59:59.999999"
    assert len(_build_partition_timestamps(from_ts, to_ts, "@hourly")) == 25


def test_add_post_partitions():
    partitions = ["year=2022/month=1"]
    post_partitions = [["wiki=en", "wiki=zh"]]
    assert add_post_partitions(partitions, post_partitions) == [
        "year=2022/month=1/wiki=en",
        "year=2022/month=1/wiki=zh",
    ]


def test_druid_intervals():
    intervals = druid_intervals("2024-01-01", "2024-02-01", "@daily")
    assert len(intervals) == 31
    assert intervals[0] == pendulum.datetime(2024, 1, 1).isoformat() + "/" + pendulum.datetime(2024, 1, 2).isoformat()
    assert intervals[1] == pendulum.datetime(2024, 1, 2).isoformat() + "/" + pendulum.datetime(2024, 1, 3).isoformat()
    assert intervals[-1] == pendulum.datetime(2024, 1, 31).isoformat() + "/" + pendulum.datetime(2024, 2, 1).isoformat()


def test_daily_partitions():
    partitions = daily_partitions("some.table", "@hourly")
    assert len(partitions) == 24
    assert partitions[0] == (
        "some.table/year={{ data_interval_start.year }}/"
        "month={{ data_interval_start.month }}/day={{ data_interval_start.day }}/"
        "hour=0"
    )
    assert partitions[-1] == (
        "some.table/year={{ data_interval_start.year }}/"
        "month={{ data_interval_start.month }}/day={{ data_interval_start.day }}/"
        "hour=23"
    )


def test_daily_partitions_with_prepartitions():
    partitions = daily_partitions("some.table", "@hourly", [["a=1", "a=2"]])
    assert len(partitions) == 48
    assert partitions[0] == (
        "some.table/a=1/year={{ data_interval_start.year }}/"
        "month={{ data_interval_start.month }}/day={{ data_interval_start.day }}/"
        "hour=0"
    )
    assert partitions[23] == (
        "some.table/a=1/year={{ data_interval_start.year }}/"
        "month={{ data_interval_start.month }}/day={{ data_interval_start.day }}/"
        "hour=23"
    )
    assert partitions[24] == (
        "some.table/a=2/year={{ data_interval_start.year }}/"
        "month={{ data_interval_start.month }}/day={{ data_interval_start.day }}/"
        "hour=0"
    )
    assert partitions[47] == (
        "some.table/a=2/year={{ data_interval_start.year }}/"
        "month={{ data_interval_start.month }}/day={{ data_interval_start.day }}/"
        "hour=23"
    )


def test_hours_of_month():
    # Regular test.
    hours = hours_of_month(pendulum.datetime(2024, 1, 1))
    assert len(hours) == 24 * 31
    assert hours[0] == pendulum.datetime(2024, 1, 1)
    assert hours[1] == pendulum.datetime(2024, 1, 1, 1)
    assert hours[24] == pendulum.datetime(2024, 1, 2)
    assert hours[-1] == pendulum.datetime(2024, 1, 31, 23)
    # Test passing a datetime that is not the beginning of a month.
    hours = hours_of_month(pendulum.datetime(2024, 1, 12, 3, 54, 37, 120))
    assert len(hours) == 24 * 31
    assert hours[0] == pendulum.datetime(2024, 1, 1)
