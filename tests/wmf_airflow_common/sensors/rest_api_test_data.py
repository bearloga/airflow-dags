import os
from datetime import datetime
from pathlib import Path

from airflow_client.client.api_client import ApiClient
from airflow_client.client.model.class_reference import ClassReference
from airflow_client.client.model.collection_info import CollectionInfo
from airflow_client.client.model.dag_run import DAGRun
from airflow_client.client.model.dag_run_collection import DAGRunCollection
from airflow_client.client.model.dag_state import DagState
from airflow_client.client.model.task import Task
from airflow_client.client.model.task_collection import TaskCollection
from airflow_client.client.model.task_extra_links import TaskExtraLinks
from airflow_client.client.model.task_instance import TaskInstance
from airflow_client.client.model.task_instance_collection import TaskInstanceCollection
from airflow_client.client.model.time_delta import TimeDelta


class FakeResponse:
    """Wrapper for JSON data, to mimic HTTP response objects."""

    def __init__(self, _data):
        self.data = _data


def deserialize_json(json_filename, allowed_types):
    # .json files are in same directory as this Python file
    current_path = Path(os.path.dirname(os.path.realpath(__file__)))
    json_path = current_path / json_filename

    client = ApiClient()

    return client.deserialize(FakeResponse(json_path.read_text()), allowed_types, True)


task_collection_allowed_types = [
    TaskCollection,
    Task,
    ClassReference,
    TaskExtraLinks,
    str,
    datetime,
    bool,
    int,
    dict,
]

dag_run_collection_allowed_types = [
    DAGRunCollection,
    DAGRun,
    DagState,
    str,
    datetime,
    bool,
    int,
    dict,
]

taskinstance_collection_allowed_types = [
    TaskInstanceCollection,
    TaskInstance,
    CollectionInfo,
    TimeDelta,
    str,
    datetime,
    bool,
    int,
    dict,
]


def get_task_collection_data(dag_id: str):
    return (
        deserialize_json(f"{dag_id}_tasks.json", task_collection_allowed_types),
        [
            "content_gap_features",
            "content_gap_metrics",
            "knowledge_gaps_sensors.wait_for_article_features",
            "knowledge_gaps_sensors.wait_for_mediawiki_page_history_snapshot",
        ],
    )


def get_dag_run_data(dag_id: str):
    return (
        deserialize_json(f"{dag_id}_dagruns.json", dag_run_collection_allowed_types),
        [
            "scheduled__2023-12-01T00:00:00+00:00",
            "scheduled__2024-01-01T00:00:00+00:00",
            "scheduled__2024-02-01T00:00:00+00:00",
            "scheduled__2024-03-01T00:00:00+00:00",
            "scheduled__2024-04-01T00:00:00+00:00",
            "scheduled__2024-05-01T00:00:00+00:00",
            "scheduled__2024-06-01T00:00:00+00:00",
        ],
    )


def get_taskinstance_data(dag_id: str):
    return (
        deserialize_json(f"{dag_id}_taskinstances.json", taskinstance_collection_allowed_types),
        [
            "content_gap_features",
            "content_gap_metrics",
            "knowledge_gaps_sensors.wait_for_article_features",
            "knowledge_gaps_sensors.wait_for_mediawiki_page_history_snapshot",
        ],
    )
