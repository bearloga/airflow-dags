import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "browser", "browser_metrics_weekly_dag.py"]


def browser_all_sites_weekly_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="browser_metrics_weekly")
    assert dag is not None
    assert len(dag.tasks) == 6
