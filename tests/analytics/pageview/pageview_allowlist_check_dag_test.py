import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "pageview", "pageview_allowlist_check_dag.py"]


def test_pageview_allowlist_check_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="pageview_allowlist_check")
    assert dag is not None
    assert len(dag.tasks) == 4
