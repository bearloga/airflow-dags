import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "table_maintenance", "table_maintenance_iceberg_dag.py"]


def test_table_maintenance_daily_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="table_maintenance_iceberg_daily")
    assert dag is not None
    assert len(dag.tasks) == 4


def test_table_maintenance_weekly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="table_maintenance_iceberg_weekly")
    assert dag is not None
    assert len(dag.tasks) == 0  # this DAG is empty for now until we add Iceberg tables that require weekly maintenance


def test_table_maintenance_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="table_maintenance_iceberg_monthly")
    assert dag is not None
    assert len(dag.tasks) == 4
