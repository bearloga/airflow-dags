import os

import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "dumps", "dumps_reconcile_wikitext_raw_daily_dag.py"]


def test_dumps_reconcile_wikitext_raw_daily_dag_loaded(dagbag, mocker, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="dumps_reconcile_wikitext_raw_daily")
    assert dag is not None
    assert dag.dag_id == "dumps_reconcile_wikitext_raw_daily"
    assert len(dag.tasks) == 2  # Task groups count as 1 task

    # Mock the output of fetch_dblist()
    base_path = os.path.dirname(__file__)
    dblist_include_fixture_path = os.path.join(base_path, "dblist_include_fixture.txt")
    dblist_exclude_fixture_path = os.path.join(base_path, "dblist_exclude_fixture.txt")
    with open(dblist_include_fixture_path, "r") as file:
        dblist_include_fixture_content = file.read()

    with open(dblist_exclude_fixture_path, "r") as file:
        dblist_exclude_fixture_content = file.read()

    mock_fetch_dblist_call(
        mocker,
        [
            dblist_include_fixture_content,
            dblist_exclude_fixture_content,
        ],
    )

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(days=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test the task in charge of getting the dblist
    calculate_effective_dblist_ti = create_and_run_task("calculate_effective_dblist", dagrun)
    result = calculate_effective_dblist_ti.xcom_pull(task_ids=calculate_effective_dblist_ti.task.task_id)
    assert len(result) == 3
    assert result == ["abwiki", "acewiki", "zuwiki"]

    # Test the spark_consistency_check task
    spark_consistency_check = dag.get_task("reconcile.spark_consistency_check")
    spark_consistency_check_ti = TaskInstance(spark_consistency_check, run_id=dagrun.run_id, map_index=0)
    spark_consistency_check_ti.dag_run = dagrun
    context = spark_consistency_check_ti.get_template_context()
    spark_consistency_check_ti.render_templates(context=context)
    skein_hook = spark_consistency_check._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    fixture_id = "analytics_dags_dumps_dumps_reconcile_wikitext_raw_daily_dag.py-reconcile_spark_consistency_check"
    compare_with_fixture(group="spark_skein_specs", fixture_id=fixture_id, **kwargs)


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


def mock_fetch_dblist_call(mocker, content_list):
    mock_one = mocker.Mock()
    mock_one.status_code = 200
    mock_one.text = content_list[0]

    mock_two = mocker.Mock()
    mock_two.status_code = 200
    mock_two.text = content_list[1]

    mock_get = mocker.patch("analytics.dags.dumps.dumps_reconcile_wikitext_raw_daily_dag.requests.get")
    mock_get.side_effect = [mock_one, mock_two]
