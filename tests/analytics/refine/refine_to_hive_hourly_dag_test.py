import os

import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType

from analytics.dags.refine.refine_to_hive_hourly_dag_factory import (
    SPARK_JOB_SCALE_XCOM_KEY,
)
from wmf_airflow_common.util import convert_dict_to_list


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "refine", "refine_to_hive_hourly_dag.py"]


def test_refine_to_hive_hourly_dag_loaded(dagbag, mocker, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag_id = "refine_to_hive_hourly"
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == 11  # The task groups are not counted as tasks.

    # Mock the event-stream-config request
    event_stream_config_fixture = os.path.join(os.path.dirname(__file__), "event_stream_config_fixture.json")
    with open(event_stream_config_fixture, "r") as file:
        mocker.patch(
            "analytics.dags.refine.refine_to_hive_hourly_dag_factory.fsspec.open",
            mocker.mock_open(read_data=file.read()),
        )

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(hours=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test the task in charge of getting the configuration
    fetch_datasets_configurations_ti = create_and_run_task("fetch_stream_configurations", dagrun)
    configurations = fetch_datasets_configurations_ti.xcom_pull(task_ids=fetch_datasets_configurations_ti.task.task_id)
    assert len(configurations) == 7
    ios_search_config = next((c for c in configurations if c["stream"] == "ios.search"), None)
    assert ios_search_config == {
        "diff_hive_table": "event.ios_search",
        "hdfs_source_paths": [
            "/wmf/data/raw/event/eqiad.ios.search/year=2024/month=08/day=08/hour=17",
            "/wmf/data/raw/event/codfw.ios.search/year=2024/month=08/day=08/hour=17",
        ],
        "hive_partition_columns": {
            "datacenter": "STRING",
            "day": "LONG",
            "hour": "LONG",
            "month": "LONG",
            "year": "LONG",
        },
        "hive_partition_paths": [
            "datacenter=eqiad/year=2024/month=8/day=8/hour=17",
            "datacenter=codfw/year=2024/month=8/day=8/hour=17",
        ],
        "hive_pre_partitions": ["datacenter=eqiad", "datacenter=codfw"],
        "hive_table": "event_alt.ios_search",
        "schema_uri": "/analytics/mobile_apps/ios_search/latest",
        "spark_refine_job_scale": "small",
        "spark_refine_job_scale_params": {"driver_cores": 2, "driver_memory": "4G", "master": "local[1]"},
        "stream": "ios.search",
        "table_format": "hive",
        "table_location": "hdfs://analytics-hadoop/user/analytics/event_alt/ios_search",
        "transform_functions": [
            "org.wikimedia.analytics.refinery.job.refine.remove_canary_events",
            "org.wikimedia.analytics.refinery.job.refine.deduplicate",
            "org.wikimedia.analytics.refinery.job.refine.geocode_ip",
            "org.wikimedia.analytics.refinery.job.refine.parse_user_agent",
            "org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain",
            "org.wikimedia.analytics.refinery.job.refine.add_normalized_host",
            "org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",
        ],
    }
    eventlogging_virtualpageview_config = next(
        (c for c in configurations if c["stream"] == "eventlogging_VirtualPageView"), None
    )
    assert eventlogging_virtualpageview_config == {
        "diff_hive_table": "event.VirtualPageView",
        "hdfs_source_paths": [
            "/wmf/data/raw/eventlogging_legacy/eventlogging_VirtualPageView/year=2024/month=08/day=08/hour=17"
        ],
        "hive_partition_columns": {"day": "LONG", "hour": "LONG", "month": "LONG", "year": "LONG"},
        "hive_partition_paths": ["year=2024/month=8/day=8/hour=17"],
        "hive_pre_partitions": [],
        "hive_table": "event_alt.VirtualPageView",
        "schema_uri": "/analytics/legacy/virtualpageview/latest",
        "spark_refine_job_scale": "large",
        "spark_refine_job_scale_params": {
            "driver_cores": 2,
            "driver_memory": "4G",
            "executor_cores": 4,
            "executor_memory": "8G",
            "master": "yarn",
            "max_executors": 32,
        },
        "stream": "eventlogging_VirtualPageView",
        "table_format": "hive",
        "table_location": "hdfs://analytics-hadoop/user/analytics/event_alt/VirtualPageView",
        "transform_functions": [
            "org.wikimedia.analytics.refinery.job.refine.filter_allowed_domains",
            "org.wikimedia.analytics.refinery.job.refine.remove_canary_events",
            "org.wikimedia.analytics.refinery.job.refine.deduplicate",
            "org.wikimedia.analytics.refinery.job.refine.geocode_ip",
            "org.wikimedia.analytics.refinery.job.refine.parse_user_agent",
            "org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain",
            "org.wikimedia.analytics.refinery.job.refine.add_normalized_host",
            "org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",
        ],
    }

    # Test the task in charge of preparing the sensor (map_index=3 is ios.search stream)
    prepare_import_paths_urls_ti = create_and_run_task(
        "refine_hive_dataset.prepare_import_paths_urls", dagrun, map_index=3
    )
    assert prepare_import_paths_urls_ti.xcom_pull(
        task_ids=prepare_import_paths_urls_ti.task.task_id, map_indexes=3
    ) == [
        "hdfs://analytics-hadoop/wmf/data/raw/event/eqiad.ios.search/year=2024/month=08/day=08/hour=17/_IMPORTED",
        "hdfs://analytics-hadoop/wmf/data/raw/event/codfw.ios.search/year=2024/month=08/day=08/hour=17/_IMPORTED",
    ]

    # Test that the rendered map_index is correct
    assert prepare_import_paths_urls_ti.rendered_map_index == "ios.search"

    # Test the task in charge of preparing the parameters for the evolve hive table job
    prepare_evolve_table_script_ti = create_and_run_task(
        "refine_hive_dataset.prepare_evolve_table_script", dagrun, map_index=3
    )
    assert prepare_evolve_table_script_ti.xcom_pull(
        task_ids=prepare_evolve_table_script_ti.task.task_id, map_indexes=3
    ) == convert_dict_to_list(
        {
            "--table": "event_alt.ios_search",
            "--schema_uri": "/analytics/mobile_apps/ios_search/latest",
            "--location": "hdfs://analytics-hadoop/user/analytics/event_alt/ios_search",
            "--transform_functions": "org.wikimedia.analytics.refinery.job.refine.remove_canary_events,org.wikimedia.analytics.refinery.job.refine.deduplicate,org.wikimedia.analytics.refinery.job.refine.geocode_ip,org.wikimedia.analytics.refinery.job.refine.parse_user_agent,org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain,org.wikimedia.analytics.refinery.job.refine.add_normalized_host,org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",  # noqa: E501
            "--partition_columns": "datacenter:STRING,year:LONG,month:LONG,day:LONG,hour:LONG",
            "--dry_run": "false",
        }
    )

    # Test the task which call the evolve table job
    evolve_table = dag.get_task("refine_hive_dataset.evolve_hive_table")
    evolve_table_ti = TaskInstance(evolve_table, run_id=dagrun.run_id, map_index=3)
    evolve_table_ti.dag_run = dagrun
    context = evolve_table_ti.get_template_context()
    evolve_table_ti.render_templates(context=context)
    skein_hook = evolve_table._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="skein_operator_spec", fixture_id=f"{dag_id}.evolve_hive_table", **kwargs)

    # Test the task in charge of preparing the parameters for the Refine job
    prepare_application_args_ti = create_and_run_task(
        "refine_hive_dataset.prepare_refine_task_params", dagrun, map_index=3
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_application_args_ti.task.task_id, map_indexes=3
    ) == convert_dict_to_list(
        {
            "--input_paths": "/wmf/data/raw/event/eqiad.ios.search/year=2024/month=08/day=08/hour=17,/wmf/data/raw/event/codfw.ios.search/year=2024/month=08/day=08/hour=17",  # noqa: E501
            "--schema_uri": "/analytics/mobile_apps/ios_search/latest",
            "--table": "event_alt.ios_search",
            "--transform_functions": "org.wikimedia.analytics.refinery.job.refine.remove_canary_events,org.wikimedia.analytics.refinery.job.refine.deduplicate,org.wikimedia.analytics.refinery.job.refine.geocode_ip,org.wikimedia.analytics.refinery.job.refine.parse_user_agent,org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain,org.wikimedia.analytics.refinery.job.refine.add_normalized_host,org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",  # noqa: E501
            "--partition_paths": "datacenter=eqiad/year=2024/month=8/day=8/hour=17,datacenter=codfw/year=2024/month=8/day=8/hour=17",  # noqa: E501
            "--spark_job_scale": "small",
        }
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_application_args_ti.task.task_id, map_indexes=3, key=SPARK_JOB_SCALE_XCOM_KEY
    ) == {"driver_cores": 2, "driver_memory": "4G", "master": "local[1]"}

    # Test the large scale Spark job params (map_index=2 is VirtualPageView)
    prepare_large_application_args_ti = create_and_run_task(
        "refine_hive_dataset.prepare_refine_task_params", dagrun, map_index=2
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_large_application_args_ti.task.task_id, map_indexes=2, key=SPARK_JOB_SCALE_XCOM_KEY
    ) == {
        "master": "yarn",
        "max_executors": 32,
        "driver_cores": 2,
        "driver_memory": "4G",
        "executor_cores": 4,
        "executor_memory": "8G",
    }

    # Test the task in charge of actually refining the dataset
    refine_hourly = dag.get_task("refine_hive_dataset.refine_to_hive_hourly")
    refine_hourly_ti = TaskInstance(refine_hourly, run_id=dagrun.run_id, map_index=2)
    refine_hourly_ti.dag_run = dagrun
    context = refine_hourly_ti.get_template_context()
    refine_hourly_ti.render_templates(context=context)
    skein_hook = refine_hourly._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id=f"{dag_id}.refine_to_hive_hourly", **kwargs)

    # Test prepare_legacy_partition_names
    prepare_legacy_partition_names_ti = create_and_run_task(
        "refine_hive_dataset.prepare_legacy_partition_names", dagrun, map_index=3
    )
    hive_partition_ending = "year=2024/month=8/day=8/hour=17"
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_legacy_partition_names_ti.task.task_id, map_indexes=3
    ) == [
        f"event.ios_search/datacenter=eqiad/{hive_partition_ending}",
        f"event.ios_search/datacenter=codfw/{hive_partition_ending}",
    ]

    # test prepare_diff_task_params
    prepare_diff_task_params_ti = create_and_run_task(
        "refine_hive_dataset.prepare_diff_task_params", dagrun, map_index=3
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_diff_task_params_ti.task.task_id, map_indexes=3
    ) == convert_dict_to_list(
        {
            "--legacy_table": "event.ios_search",
            "--new_table": "event_alt.ios_search",
            "--new_table_format": "hive",
            "--year": 2024,
            "--month": 8,
            "--day": 8,
            "--hour": 17,
        }
    )

    # test diff_check
    diff_check = dag.get_task("refine_hive_dataset.diff_check")
    diff_check_ti = TaskInstance(diff_check, run_id=dagrun.run_id, map_index=3)
    diff_check_ti.dag_run = dagrun
    context = diff_check_ti.get_template_context()
    diff_check_ti.render_templates(context=context)
    skein_hook = diff_check._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id=f"{dag_id}.diff_check", **kwargs)


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance
