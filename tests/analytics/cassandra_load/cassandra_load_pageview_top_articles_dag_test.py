import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "cassandra_load", "cassandra_load_pageview_top_articles_dag.py"]


def test_cassandra_load_pageview_top_articles_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="cassandra_load_pageview_top_articles_daily")
    assert dag is not None
    assert len(dag.tasks) == 2
    dag = dagbag.get_dag(dag_id="cassandra_load_pageview_top_articles_monthly")
    assert dag is not None
    assert len(dag.tasks) == 2
