import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "webrequest", "actor", "webrequest_actor_metrics_hourly_dag.py"]


def test_webrequest_actor_metrics_hourly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="webrequest_actor_metrics_hourly")
    assert dag is not None
    assert len(dag.tasks) == 2
